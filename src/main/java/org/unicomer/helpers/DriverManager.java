package org.unicomer.helpers;

import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class DriverManager {

    private static DriverManager instance;
    private WebDriver driver;

    private DriverManager(){
        System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
        ChromeOptions co = new ChromeOptions();
        co.addArguments("start-maximized");
        co.addArguments("incognito");
        co.addArguments("remote-allow-origins=*");
        co.addArguments("--disable-gpu", "--remote-allow-origins=*", "--no-sandbox");
        this.driver = new ChromeDriver(co);
        //browserOptions();
    }

    private void browserOptions(){
        ChromeOptions browserOptions = new ChromeOptions();
        browserOptions.setPlatformName("Windows 11");
        browserOptions.setBrowserVersion("latest");
        browserOptions.addArguments("start-maximized");
        browserOptions.addArguments("incognito");
        browserOptions.addArguments("remote-allow-origins=*");
        browserOptions.addArguments("--disable-gpu", "--remote-allow-origins=*", "--no-sandbox");
        Map<String, Object> sauceOptions = new HashMap<>();
        sauceOptions.put("username", "rigoberto_campos");
        sauceOptions.put("accessKey", "245c58f9-837c-49a1-a810-b78e30e84e73");
        sauceOptions.put("build", "selenium-build-WF84J");
        sauceOptions.put("name", "testing Sauce");
        browserOptions.setCapability("sauce:options", sauceOptions);

        URL url = null;
        try {
            url = new URL("https://ondemand.us-west-1.saucelabs.com:443/wd/hub");
            this.driver = new RemoteWebDriver(url, browserOptions);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }

    }

    public static synchronized DriverManager getInstance(){
        if(instance == null){
            instance = new DriverManager();
        }
        return instance;
    }

    public WebDriver getDriver(){
        return driver;
    }

}
