package org.unicomer.helpers;

import com.assertthat.selenium_shutterbug.core.Capture;
import com.assertthat.selenium_shutterbug.core.Shutterbug;
import org.unicomer.constants.SystemConstants;
import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.webelements.Checkbox;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.unicomer.utils.documents.DocumentManager;

public class PageCommonAction extends PageObject {

    private String keys;
    private int actionTries = 1;

    public PageCommonAction(){
        setChromeDriver();
    }

    private synchronized void setChromeDriver(){
        setDriver(DriverManager.getInstance().getDriver());
    }

    public WebElementFacade findElement(String opt, String value){
        switch (opt){
            case SystemConstants.BY_ID:
               return $(By.id(value));
            case SystemConstants.BY_NAME:
                return $(By.name(value));
            case SystemConstants.BY_XPATH:
                return $(By.xpath(value));
            default:
                return null;
        }
    }

    public ListOfWebElementFacades findElements(String xpath){
        ListOfWebElementFacades list = findAll(By.xpath(xpath));

        return list;
    }

    private void checkElement(WebElementFacade element){
        Checkbox ch = new Checkbox((WebElement)element);
        if(!ch.isChecked()){
            ch.setChecked(true);
        }
    }

    public void doAction(String opt, WebElementFacade element){
        try{
            centerElement(element);
            switch (opt){
                case SystemConstants.PA_CLICK:
                    clickElement(waitForClick(element));
                    break;
                case SystemConstants.PA_SEND_KEYS:
                    setText(element, this.keys);
                    break;
                case SystemConstants.PA_CHECK:
                    checkElement(element);
                    break;
                case SystemConstants.PA_JS_CLICK:
                    jsClick(element);
                    break;
                case SystemConstants.PA_DELETE_KEYS:
                    element.clear();
                    break;
            }

        }catch (StaleElementReferenceException | ElementClickInterceptedException e){
            if(actionTries > 4){
                actionTries = 0;
                return;
            }else{

                actionTries = actionTries + 1;
                doAction(opt,element);
            }
        }
    }

    public void takeScreenShoot(){
        try {
            ((JavascriptExecutor) getDriver()).executeScript("document.getElementById('gwt-debug-headerHolder').style.position = 'static';");
        }catch (JavascriptException | NoSuchElementException e){

        }
        try {

            Shutterbug.
                    shootPage(getDriver(), Capture.FULL,264,true).
                    withName("image_"+ DocumentManager.getInstance().getCount()).
                    save();
            DocumentManager.getInstance().
                    addImage("screenshots/image_"+ DocumentManager.getInstance().getCount());
            System.out.println("ScreenShot DONEE");
        }catch (JavascriptException | NoSuchElementException e){

            e.printStackTrace();
        }
    }

    public void doAction(String opt, WebElementFacade element, String keys){
        this.keys = keys;
        doAction(opt, element);
    }

    public void doAction(ListOfWebElementFacades list, String action){
        for(int i = 0; i<list.size(); i++){
            doAction(action, list.get(i));
        }
    }

    public void cambiarT(){

        WebElementFacade e = findElement(SystemConstants.BY_XPATH, "//span[@debugid='userName']");
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].innerText = ' '", e);
    }

    public void navigateTo(String url){
        //getDriver().navigate().to(url);
        //navigateTo(url);
        openAt(url);
    }

    private void setText(WebElementFacade element, String text){
        element.type(text);
    }

    private void clickElement(WebElementFacade element){
        element.click();
    }

    public void centerElement(WebElementFacade element){
        String scrollElementIntoMiddle = "arguments[0].scrollIntoView({'block':'center','inline':'center'})";

        ((JavascriptExecutor) getDriver()).executeScript(scrollElementIntoMiddle, element);
    }

    private void jsClick(WebElementFacade element ){
        JavascriptExecutor executor = (JavascriptExecutor)getDriver();
        executor.executeScript("arguments[0].click();", element);
    }

    private WebElementFacade waitForClick(WebElementFacade element){
        return (WebElementFacade)waitForCondition().until(
                ExpectedConditions.elementToBeClickable(element)
        );
    }

    public void waitForUrl(String url){
        try {
            waitForCondition().until(ExpectedConditions.urlContains(url));
        }catch (TimeoutException e){
            navigateTo(url);
        }
    }

    public void waitForElementVisibility(WebElementFacade element){
        try {
            element.waitUntilVisible();
        }catch (TimeoutException e){
            return;
        }
    }

    public boolean isElementOnScreen(WebElementFacade element){
        try {

            if(element.isDisplayed()){
                return isElementOnScreen(element);
            }
            else return false;
        }catch (StaleElementReferenceException | NoSuchElementException e){
            return false;
        }
    }

    public boolean isElementPresent(WebElementFacade element){
        try {

            if(element.isDisplayed()){
                return true;
            }
            else return false;
        }catch (StaleElementReferenceException | NoSuchElementException e){
            return false;
        }
    }

    public boolean waitElementOnScreen(WebElementFacade element){
        try {
            if(!element.isDisplayed()){
                waitElementOnScreen(element);
            }
            return true;
        }catch (StaleElementReferenceException | NoSuchElementException e){
            return waitElementOnScreen(element);
        }
    }

    public WebDriver getCommonDriver(){
        return getDriver();
    }

    public void saveWebData(String key, String val){
        DocumentManager.getInstance().addMapData(key, val);
    }

}
