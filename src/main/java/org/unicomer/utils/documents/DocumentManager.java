package org.unicomer.utils.documents;


import org.apache.poi.ss.usermodel.Sheet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class DocumentManager {

    private static DocumentManager instance;
    private ArrayList<String> images;

    private String valueChannel;

    private int count=1;

    private String title="";

    private Map<String, String> webData;

    private Map<String, String> keyAmounts;

    private String notas="";

    private DocumentManager(){
        images = new ArrayList<String>();
        webData = new LinkedHashMap<String,String>();
        keyAmounts = new HashMap<String,String>();
    }

    public synchronized static DocumentManager getInstance(){
        if(instance == null){
            instance = new DocumentManager();
        }
        return instance;
    }

    public void addImage(String path){
        images.add(path);
        count = count + 1;
    }

    public void insertAllImagePOI(WordDocument document){
        for(int i =0; i<images.size(); i++){
            document.insertImage(images.get(i));
        }
        images.clear();
    }

    public String parseAmount (String a) {
        a = a.substring(2);
        a = a.replaceAll("[^\\w+]", "");
        int lap = Integer.parseInt(a);
        String lap2 = String.format("%015d", lap);
        return lap2;
    }
    public String parseDate (String date) {
        date = firstNChars(date,10);
        date = date.replaceAll("-", "");
        return date;
    }
    public String firstNChars(String str, int n) {
        if (str == null) {
            return null;
        }

        return str.length() < n ? str : str.substring(0, n);
    }

    public void createNormalDocument(String title){
        WordDocument wd = new WordDocument(title);
        insertAllImagePOI(wd);
        wd.addNotes(this.notas);
        wd.outPutDocument();
    }

    public ExcelDocument createXlsxDoc(String sheetName){
        ExcelDocument excel = new ExcelDocument();
        excel.createSheet(sheetName);
        return excel;
    }

    public void addMapData(String key, String val){
        if(key.equals("Activation Date"))
            val = parseDate (val);
        if (key.equals("Loan Amount"))
            val = parseAmount(val);
        if (key.equals("Principal Balance"))
            val = parseAmount(val);
        System.out.println(key);
        webData.put(key,val);
    }

    public void addDataToExcel(ExcelDocument excel, int start, int end){
        Sheet sheet = excel.getActualSheet();
        //excel.addRow(sheet);
        webData.forEach((key, val) -> {
            excel.addRow(sheet);
            excel.setColumnCount(start);
            excel.addData(key);
            excel.setColumnCount(end);
            excel.addData(val);
        });
    }

    public void saveExcelDoc(ExcelDocument excel){
        excel.saveFile();
    }

    public int getCount(){
        return count;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title =  title;
    }

    public void setAccountAmounts(String key, String values){
        keyAmounts.put(key, values);
    }

    public String getAccountValue(String key){
        return keyAmounts.get(key);
    }

    public void setNotas(String notas){
        this.notas = this.notas +" "+notas;
    }

}
