package org.unicomer.utils.documents;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class WordDocument {

    private XWPFDocument document;
    private String name;

    public WordDocument(String title){
        this.document = new XWPFDocument();
        this.name = title;
        createTitle(title);
    }

    public void createTitle (String t) {
        XWPFParagraph title = document.createParagraph();
        title.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun titleRun = title.createRun();
        titleRun.setText(t);
        titleRun.setColor("000000");
        titleRun.setBold(true);
        titleRun.setFontFamily("Arial");
        titleRun.setFontSize(12);
        XWPFParagraph subTitle = document.createParagraph();
        subTitle.setAlignment(ParagraphAlignment.CENTER);
    }

    public void insertImage(String path) {
        XWPFParagraph picture = document.createParagraph();
        picture.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun imageRun = picture.createRun();
        try {
            FileInputStream inputStream = new  FileInputStream(new File(path+".png"));
            imageRun.addPicture(inputStream,
                    XWPFDocument.PICTURE_TYPE_PNG, path,
                    Units.toEMU(445), Units.toEMU(350));
        }catch(IOException | InvalidFormatException e){
        }
    }

    public void addNotes(String n){
        XWPFParagraph note = document.createParagraph();
        note.setAlignment(ParagraphAlignment.RIGHT);
        XWPFRun noteRun = note.createRun();
        noteRun.setText(n);
    }

    public void outPutDocument(){
        File currDir = new File(this.name);
        String path = currDir.getAbsolutePath();
        String fileLocation = path + ".docx";

        try{
            FileOutputStream out = new FileOutputStream(fileLocation);
            document.write(out);
            out.close();
            document.close();
        }catch(IOException e){

        }
    }
}
