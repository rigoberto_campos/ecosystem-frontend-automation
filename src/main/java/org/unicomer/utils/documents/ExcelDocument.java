package org.unicomer.utils.documents;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
public class ExcelDocument {

    private Workbook workbook;
    private int rowCount = 1;
    private int columnCount;
    private Row row;

    private String sheetName;

    public ExcelDocument(){
        workbook = new XSSFWorkbook();
    }

    public void saveFile(){
        File currDir = new File(".");
        String path = currDir.getAbsolutePath();
        String fileLocation = path.substring(0, path.length() - 1) +sheetName+ ".xlsx";

        try{
            FileOutputStream outputStream = new FileOutputStream(fileLocation);
            workbook.write(outputStream);
            workbook.close();
        }catch(IOException e){

        }
    }

    public void addData(String cellValue){
        //CellStyle style = workbook.createCellStyle();
        //style.setWrapText(true);

        Cell cell = row.createCell(columnCount);
        cell.setCellValue(cellValue);
        //cell.setCellStyle(style);
    }

    public void addRow(Sheet sheet){
        rowCount = rowCount + 1;
        row = sheet.createRow(rowCount);
    }

    public Sheet createSheet(String sheetName){
        this.sheetName = sheetName;
        Sheet sheet = workbook.createSheet(sheetName);
        sheet.setColumnWidth(0, 6000);
        return sheet;
    }

    public Sheet getActualSheet(){
        return workbook.getSheet(this.sheetName);
    }
    public void setColumnCount(int num){
        this.columnCount = num;
    }
}
