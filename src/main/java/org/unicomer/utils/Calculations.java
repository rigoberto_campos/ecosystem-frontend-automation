package org.unicomer.utils;

public class Calculations {

    private static Calculations instance;

    private Calculations(){

    }

    public static synchronized  Calculations getInstance(){
        if(instance == null){
            instance = new Calculations();
        }
        return  instance;
    }

    public void cpiFromLoanCalculation(String loan){
        double loanValue = Double.valueOf(loan);
        double cpiResult = loanValue * 0.0471;

        System.out.println(cpiResult);
    }

    public void cpiTaxFromLoanCalculation(String loan){
        double loanValue = Double.valueOf(loan);
        double cpiResult = loanValue * 0.007065;

        System.out.println(cpiResult);
    }

    public void principalBalanceCalculation(String loan){
        double loanValue = Double.valueOf(loan);
        double cpiResult = loanValue * 0.007065;

        System.out.println(cpiResult);
    }
}
