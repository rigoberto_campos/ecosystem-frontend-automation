package org.unicomer.constants;

public final class SystemConstants {
    public static final String PA_CLICK = "1";
    public static final String PA_SEND_KEYS = "2";
    public static final String PA_CHECK = "3";
    public static final String PA_JS_CLICK = "4";

    public static final String PA_DELETE_KEYS = "5";

    public static final String BY_ID = "1";
    public static final String BY_NAME = "2";
    public static final String BY_XPATH = "3";
    public static final String BY_LEFT_SELECTOR = "4";
    public static final String MAMBU_URL = "MAMBU.URL";
    public static final String MAMBU_USERNAME = "MAMBU.USERNAME";
    public static final String MAMBU_PASSWORD = "MAMBU.PASSWORD";
    public static final String EMMA_USERNAME = "EMMA.USERNAME";
    public static final String EMMA_PASSWORD = "EMMA.PASSWORD";
    public static final String EMMA_URL = "EMMA.URL";
    public static final String WORKF_URL = "WORKF.URL";
    public static final String WORKF_USERNAME = "WORKF.USERNAME";
    public static final String WORKF_PASSWORD = "WORKF.PASSWORD";

    public static final String CYBER_URL ="CYBER.URL";
    public static final String CYBER_USERNAME = "CYBER.USERNAME";
    public static final String CYBER_PASSWORD = "CYBER.PASS";
    public static final String SEARCH_ACOUNT = "Acount";
    public static final String SEARCH_ADDRESS = "Address";
    public static final String SEARCH_TELEPHONE = "Telephone";
    public static final String SEARCH_CUSTOMER = "Customer name";

    public  static final String SMARTCREDIT_URL = "SMARTCREDIT.URL";
    public  static final String SMARTCREDIT_USERNAME = "SMARTCREDIT.USERNAME";
    public  static final String SMARTCREDIT_PASS = "SMARTCREDIT.PASS";
    public static final String DESCRIPTION_RULE_EDIT = "Edit QA Testing Automation ";

    public static final String NAME_SCORING = "NewRuleScoringAutomation";
    public static final String DESCRIPTION_SCORING = "QA Testing Automation Scoring";
    public static final String OBJECT_SCORING = "Scoring";
    public static final String DESCRIPTION_SCORING_EDIT = "Edit QA Testing Automation Scoring";

    public static final String NAME_PARAMETER = "TESTAUTOMATION";
    public static final String VALUE_PARAMETER = "TESTVALUE";
    public static final String DESCRIPTION_PARAMETER ="TEST Description";
    public static final String EDIT_DESCRIPTION_PARAMETER ="EDIT TEST Description";

}
