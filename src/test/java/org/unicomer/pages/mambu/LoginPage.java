package org.unicomer.pages.mambu;

import net.serenitybdd.core.pages.WebElementFacade;
import org.unicomer.constants.SystemConstants;
import org.unicomer.helpers.PageCommonAction;
import org.unicomer.utils.ConstantReader;

public class LoginPage extends PageCommonAction {


    public void navigate(){
        navigateTo(ConstantReader.getInstance().getProperties().get(SystemConstants.MAMBU_URL));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//img[@src='../img/mambu-loading.gif']"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//a[@href='/saml/login']"));

    }

    public void username(){
        WebElementFacade userName = findElement(SystemConstants.BY_NAME, "loginfmt");
        doAction(SystemConstants.PA_SEND_KEYS, userName,
                ConstantReader.getInstance().getProperties().get(SystemConstants.EMMA_USERNAME));
        /*
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                    "//input[@name='username']"),
                ConstantReader.getInstance().getProperties().get(SystemConstants.MAMBU_USERNAME));*/
    }

    public void password(){
        WebElementFacade password = findElement(SystemConstants.BY_XPATH, "//input[@name='passwd' and @aria-required='true']");
        doAction(SystemConstants.PA_SEND_KEYS, password,
                ConstantReader.getInstance().getProperties().get(SystemConstants.EMMA_PASSWORD));
        /*
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                        "//input[@name='password']"),
                ConstantReader.getInstance().getProperties().get(SystemConstants.MAMBU_PASSWORD));*/
    }

    public void submitCredentials(){
        //takeScreenShoot();
        WebElementFacade btnSubmit = findElement(SystemConstants.BY_ID, "idSIButton9");
        doAction(SystemConstants.PA_CLICK, btnSubmit);
        /*
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//img[@src='../img/mambu-loading.gif']"));
        isElementOnScreen(findElement(SystemConstants.BY_ID,
                "//div[@id='gwt-debug-loadingText']"));*/
    }
}
