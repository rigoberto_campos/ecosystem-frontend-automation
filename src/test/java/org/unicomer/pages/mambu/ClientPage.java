package org.unicomer.pages.mambu;

import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.NoSuchElementException;
import org.unicomer.constants.SystemConstants;
import org.unicomer.helpers.PageCommonAction;
import org.unicomer.utils.documents.DocumentManager;

public class ClientPage extends PageCommonAction {
    private  String channel = "";

    public void selectOverviewMenu(){
        WebElementFacade element = findElement(SystemConstants.BY_XPATH,
                "//div[@id='gwt-debug-tabBar']//div[@tabindex='0']//*[contains(text(),'vervie')]");

        doAction(SystemConstants.PA_CLICK, element);
        //isElementOnScreen(findElement(SystemConstants.BY_ID, "//div[@id='gwt-debug-loadingText']"));
        takeScreenShoot();

    }

    public void selectAllCheck(){
        ListOfWebElementFacades list = findElements("//input[@type='checkbox']");
        doAction(list, SystemConstants.PA_CHECK);
        //isElementOnScreen(findElement(SystemConstants.BY_ID, "//div[@id='gwt-debug-loadingText']"));
        takeScreenShoot();
    }

    public void scheduleSubMenu(){
        try {
            WebElementFacade element = findElement(SystemConstants.BY_XPATH,
                    "//a[contains(text(),'Schedule')]");
            doAction(SystemConstants.PA_CLICK, element);
            //isElementOnScreen(findElement(SystemConstants.BY_ID, "//div[@id='gwt-debug-loadingText']"));
            takeScreenShoot();

        }catch (NoSuchElementException e){

        }
    }

    public void transactionSubMenu(){
        try {
            WebElementFacade element = findElement(SystemConstants.BY_XPATH,
                    "//a[contains(text(),'Transaction')]");
            doAction(SystemConstants.PA_CLICK, element);

            WebElementFacade transaction = findElement(SystemConstants.BY_XPATH,
                    "//*[@id='gwt-debug-showReversedCheckbox-input']");
            doAction(SystemConstants.PA_CLICK, transaction);
            isElementOnScreen(findElement(SystemConstants.BY_ID, "//div[@id='gwt-debug-loadingText']"));
            takeScreenShoot();

        }catch (NoSuchElementException e){

        }
    }
    public void pagination(int num){
        try {
            WebElementFacade element = findElement(SystemConstants.BY_ID,
                    "gwt-debug-displayResultsListBox");
            doAction(SystemConstants.PA_CLICK, element);
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "//*[@id='gwt-debug-displayResultsListBox']/option[@value='"+num+"']"));

            isElementOnScreen(findElement(SystemConstants.BY_ID,
                    "//div[@id='gwt-debug-loadingText']"));

        }catch (NoSuchElementException e){

        }
    }

    public void repaymentEntered(int i){
        try {
            String td = "(//*[@id='gwt-debug-transactionsTable']/tbody[1]/tr["+i+"]/td[4]/div)";
            String val = findElement(SystemConstants.BY_XPATH,td).getText();
            if (val.equals("Repayment Entered")) {
                addDataLastPay(i);
                return;
            }
            i = i + 1;
            repaymentEntered(i);

        }catch (NoSuchElementException e){

        }
    }
    private void findRepaymentEntered(String list, int size, int num){
        if(num > size){
            return;
        }
        String td = "("+list+"["+num+"]//td//div)[1]";
        String key = findElement(SystemConstants.BY_XPATH,td).getText();
        if(!key.equals("")){
            String val = findElement(SystemConstants.BY_XPATH,"("+list+"["+num+"]//td//div)[2]").getText();
            saveWebData(key,val);
        }
        num = num + 1;
        addData(list,size, num);
    }

    public void detailsSubMenu(){
        try {
            WebElementFacade element = findElement(SystemConstants.BY_XPATH,
                    "//a[contains(text(),'Details')]");
            doAction(SystemConstants.PA_CLICK, element);
            //isElementOnScreen(findElement(SystemConstants.BY_ID, "//div[@id='gwt-debug-loadingText']"));
            takeScreenShoot();

        }catch (NoSuchElementException e){

        }
    }

    public void getName() {
        WebElementFacade element = findElement(SystemConstants.BY_XPATH,
                "//*[@debugid='clientName']");
        saveWebData("Name",element.getText());
    }

    public void getEmail() {
        try {
            WebElementFacade element = findElement(SystemConstants.BY_XPATH,
                    "//*[@id='gwt-debug-content']/tbody/tr/td[2]/a");
            saveWebData("Email", element.getText());
        } catch (NoSuchElementException e) {
            saveWebData("Email", "");
            System.out.println("El email esta vacio");
        }
    }
    public void getDocumentID() {
        WebElementFacade element = findElement(SystemConstants.BY_XPATH,
                "(//div//table[@id='gwt-debug-content'])[5]//tbody//tr//td[2]//div");
        String[] doc = element.getText().split(":");
        String docu =  doc[1].trim();
        saveWebData("Document ID",docu);
    }

    public void closedAccountMenu(){
        try {
            WebElementFacade element = findElement(SystemConstants.BY_XPATH,
                    "//div[@id='gwt-debug-tabBar']//div[@tabindex='0']//*[contains(text(),'Closed')]");
            doAction(SystemConstants.PA_CLICK, element);
            isElementOnScreen(findElement(SystemConstants.BY_ID,
                    "//div[@id='gwt-debug-loadingText']"));
        }catch (NoSuchElementException e){

        }

    }

    public void saveData(String table){
        isElementOnScreen(findElement(SystemConstants.BY_ID,
                "//div[@id='gwt-debug-loadingText']"));
        String path = "((//div//table[@id='gwt-debug-content'])["+table+"]//tbody//tr)";
        ListOfWebElementFacades list = findElements(path);
        int size = list.size();
        addData(path, size, 1);
    }

    public void saveName(String table){
        isElementOnScreen(findElement(SystemConstants.BY_ID,
                "//div[@id='gwt-debug-loadingText']"));
        String path = "((//div//table[@id='gwt-debug-content'])["+table+"]//tbody//tr)";
        ListOfWebElementFacades list = findElements(path);
        int size = list.size();
        System.out.println(size);
        addData(path, size, 1);
    }

    private void addData(String list, int size, int num){
        if(num > size){
            return;
        }
        String td = "("+list+"["+num+"]//td//div)[1]";
        String key = findElement(SystemConstants.BY_XPATH,td).getText();
        if(!key.equals("")){
            String val = findElement(SystemConstants.BY_XPATH,"("+list+"["+num+"]//td//div)[2]").getText();
            saveWebData(key,val);
        }
        num = num + 1;
        addData(list,size, num);
    }
    private void addDataLastPay(int a){
        String lastamountpaid = findElement(SystemConstants.BY_XPATH,
                "//*[@id='gwt-debug-transactionsTable']/tbody[1]/tr["+a+"]/td[5]/div").getText();
        lastamountpaid = lastamountpaid.substring(2);
        lastamountpaid = lastamountpaid.replaceAll("[^\\w+]", "");
        int lap = Integer.parseInt(lastamountpaid);
        String lap2 = String.format("%015d", lap);
        saveWebData("last amount paid",lap2);

        String lastpaymentdate = findElement(SystemConstants.BY_XPATH,
                "//*[@id='gwt-debug-transactionsTable']/tbody[1]/tr["+a+"]/td[3]/div").getText();
        lastpaymentdate = firstNChars(lastpaymentdate,10);
        lastpaymentdate = lastpaymentdate.replaceAll("-", "");
        saveWebData("last payment date",lastpaymentdate);
    }

    public String firstNChars(String str, int n) {
        if (str == null) {
            return null;
        }

        return str.length() < n ? str : str.substring(0, n);
    }
    public void getAccountAmount(){
       /* WebElementFacade element = findElement(SystemConstants.BY_XPATH,
                "(//div[contains(@id,'Total-Balance')]//div[contains(@id,'number')])[2]");

        String value = element.getText();

        value = value.substring(2);
        DocumentManager.getInstance().setAccountAmounts("Total Balance", value);*/
    }

    public void clickRePayment(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_ID,
                        "enterRepayment"));
    }

    public void clickCloseAccount(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_ID,
                        "closeAccount"));
    }

    public void paymentAmount(String amount){
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_ID,
                "amount"));
        findElement(SystemConstants.BY_XPATH,
                "//input[contains(@id,'amount')]").clear();
        /*doAction(SystemConstants.PA_SEND_KEYS,
                findElement(SystemConstants.BY_XPATH,
                        "//input[contains(@id,'amount')]"),
                DocumentManager.getInstance().getAccountValue("Total Balance"));*/

        doAction(SystemConstants.PA_SEND_KEYS,
                findElement(SystemConstants.BY_XPATH,
                        "//input[contains(@id,'amount')]"),
                amount);
        isElementOnScreen(findElement(SystemConstants.BY_ID,
                "//div[@id='gwt-debug-loadingText']"));
    }

    public void clickSelectDate(){

        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "(//select[contains(@id,'deckedListBox')])[1]"));

        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "(//option[contains(@value,'BACKDATE')])[1]"));
    }

    public void clickBackDate(){

        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "(//input[contains(@id,'backdateDate')])[2]"));
    }

    public void previousMonth(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//div[contains(@class,'datePickerPreviousButton')]"));
    }

    public void monthDay(String day){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "(//div[text()="+day+"])[1]"));
    }

    public void applyRepayment(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//span[contains(text(),'Apply Repayment')]"));

        isElementOnScreen(findElement(SystemConstants.BY_ID,
                "//div[@id='gwt-debug-loadingText']"));
    }

    public void paymentErrorAmount(String amount){

            WebElementFacade element =  findElement(SystemConstants.BY_XPATH,
                    "//div[contains(@id,'gwt-debug-amountError')]");
            String sentenceError = element.getText();
            String excedentPayment = sentenceError.replaceAll("[^\\d.]", "");
            double numeric = Double.parseDouble(amount)-Double.parseDouble(excedentPayment);
            String correctAmount = String.valueOf(numeric);
            paymentAmount(correctAmount);
    }

    public void paymentErrorAndClose(String amount){
        WebElementFacade element =  findElement(SystemConstants.BY_XPATH,
                "//div[contains(@id,'gwt-debug-amountError')]");
        String sentenceError = element.getText();
        String excedentPayment = sentenceError.replaceAll("[^\\d.]", "");
        double numeric = Double.parseDouble(amount)-Double.parseDouble(excedentPayment);
        String correctAmount = String.valueOf(Math.round(numeric/2));
        paymentAmount(correctAmount);
    }

    public void closeAccount(String day){
        isElementOnScreen(findElement(SystemConstants.BY_ID,
                "//div[@id='gwt-debug-loadingText']"));
        boolean payment = isElementPresent(findElement(SystemConstants.BY_ID,"enterRepayment"));
        while (payment){
            WebElementFacade element = findElement(SystemConstants.BY_XPATH,
                    "(//div[contains(@id,'Total-Balance')]//div[contains(@id,'number')])[2]");
            String value = element.getText();
            value = value.substring(2);
            doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_ID,"enterRepayment"));
            paymentAmount(value);
            clickSelectDate();
            clickBackDate();
            monthDay(day);
            applyRepayment();
            payment = isElementPresent(findElement(SystemConstants.BY_ID,"enterRepayment"));
        }
        clickCloseAccount();
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_ID,"gwt-debug-actionButton"));
    }

    public void validateRepayment(){
        WebElementFacade element = findElement(SystemConstants.BY_XPATH,
                "//*[@id='gwt-debug-transactionsTable']/tbody[1]/tr[1]");
        String value = element.getText();
        String[] lineas = value.split("\n");

// Asigna cada línea a una variable distinta
        String NombrePago = lineas[0];
        String Fecha = lineas[2];
        String Tipo = lineas[3];

// Muestra los valores para comprobar que están en variables distintas
        System.out.println("Nombre del pago "+NombrePago);
        System.out.println("Fecha "+Fecha);
        System.out.println("Tipo "+Tipo);
    }

    public void actionMenu(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//*[@id='gwt-debug-transactionsTable']/tbody[1]/tr[1]/td[3]/div"));

        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//*[@id='gwt-debug-transactionsTable']/tbody[1]/tr[1]/td[9]/div/button/div"));

        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_ID,
                        "viewItem"));

        WebElementFacade element = findElement(SystemConstants.BY_XPATH,
                "/html/body/div[5]/div/table/tbody/tr[2]/td[2]/div/div/div[2]/div[2]/table/tbody/tr/td[2]/div");

        takeScreenShoot();

        channel = element.getText();

        WebElementFacade elementFacade = findElement(SystemConstants.BY_XPATH,
                "//button[contains(text(),'Close')]");

        doAction(SystemConstants.PA_CLICK, elementFacade);

    }

    public void getChannel(){
        System.out.println("Canal = "+channel);
    }
}
