package org.unicomer.pages.mambu;

import org.openqa.selenium.NoSuchElementException;
import org.unicomer.constants.SystemConstants;
import org.unicomer.helpers.PageCommonAction;

public class MainPage extends PageCommonAction {


    public void searchId(String id){
        //isElementOnScreen(findElement(SystemConstants.BY_ID, "//div[@id='gwt-debug-loadingText']"));
        //takeScreenShoot();
        isElementOnScreen(findElement(SystemConstants.BY_ID,
                "//div[@id='gwt-debug-loadingText']"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_ID,
                "gwt-debug-searchInput"));
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_ID,
                "gwt-debug-searchInput"), id);

    }

    public void selectId(){
        try {
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                    "(//table[@id='gwt-debug-resultsTable']//tr)[1]"));
        }catch(NoSuchElementException e){
            selectId();
        }
        isElementOnScreen(findElement(SystemConstants.BY_ID,
                "//div[@id='gwt-debug-loadingText']"));
    }

    public void selectCashLoanAccount(){
        isElementOnScreen(findElement(SystemConstants.BY_ID,
                "//div[@id='gwt-debug-loadingText']"));
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//div[contains(@id,'Cash Loan')]"));
    }
}
