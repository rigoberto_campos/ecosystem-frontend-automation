package org.unicomer.pages.workFlow;

import org.unicomer.constants.SystemConstants;
import org.unicomer.helpers.PageCommonAction;

public class RequestTracking extends PageCommonAction {

    public void searchByTRN(String trn){

        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                "//input[@test-id='searchWorking-txt']"), trn);
    }

    public void clickSearch(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//button[@test-id='searchWorking-Btn']"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
    }

    public void clickOnRequest(){
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//button[@test-id='requestNumber-Btn']"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
    }

    public void saveData(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//button[@test-id='saveVerified-btn']"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
    }

    public void generalInfo(){
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//*[@test-id='menu-dwpn']"));
    }

    public void selectResolve(String opt){
        try {
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                    "//span[contains(text(),' "+opt+" ')]"));
            isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
        }catch (org.openqa.selenium.NoSuchElementException e) {
            generalInfo();
            selectResolve(opt);
        }
    }

    public void clickStatus(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//mat-select[@test-id='newStatus-dpw']"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
    }

    public void dataVerification(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//span[contains(text(),' Data verification ')]"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
    }

    public void subStatus(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//*[@test-id='subStatus-dpw']"));
    }

    public void dataValidation(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//span[contains(text(),' Data validation process ')]"));
    }

    public void selectApproved(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//span[contains(text(),' Approved ')]"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
        centerElement(findElement(SystemConstants.BY_XPATH,
                "(//mat-tab-body)[1]"));
        takeScreenShoot();
    }

    public void saveChange(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//button[@test-id='saveNewStatus-btn']"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
    }

    public void selectAllRequest() {
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//a[@test-id='applications-Opt']"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
    }

    public void searchDocumentRequest(String id){
        doAction(SystemConstants.PA_SEND_KEYS,
                findElement(SystemConstants.BY_XPATH,
                        "//input[@test-id='searchApplications-txt']"),
                id);
    }

    public void search(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//button[@test-id='searchApplications-Btn']"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
    }

    public void clickRequestNumber(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "(//button[@test-id='requestNumber-Btn'])[1]"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
        centerElement(findElement(SystemConstants.BY_XPATH,
                "(//mat-tab-body)[1]"));
        takeScreenShoot();
    }


}
