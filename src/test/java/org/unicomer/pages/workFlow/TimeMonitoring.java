package org.unicomer.pages.workFlow;


import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.unicomer.constants.SystemConstants;
import org.unicomer.helpers.PageCommonAction;
import org.unicomer.utils.ConstantReader;

public class TimeMonitoring extends PageCommonAction {

    public void searchByDocument(){
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//mat-select[@test-id='search-by-dwpn']"));
    }

    public void selectDocument(){
        try {
            doAction(SystemConstants.PA_CLICK,
                    findElement(SystemConstants.BY_XPATH,
                            "//span[contains(text(),'Document')]"));
        }catch (org.openqa.selenium.TimeoutException | org.openqa.selenium.NoSuchElementException toe){
            searchByDocument();
            selectDocument();
        }

    }

    public void enterDocument(String trn){
        doAction(SystemConstants.PA_SEND_KEYS,
                findElement(SystemConstants.BY_XPATH,
                        "//input[@test-id='search-txt']"),
                trn);
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
    }

    public void clickSearch(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//button[@test-id='search-btn']"));

        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
    }

    public void selectRequest(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "(//*[@test-id='reassign-all-checkbox'])[2]"));
    }

    public void assignRequest(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//button[@test-id='reassign-btn']"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
    }

    public void selectUser(){
        JavascriptExecutor executor = (JavascriptExecutor)getDriver();
        takeScreenShoot();
        executor.executeScript("arguments[0].click();",
                (WebElement) findElement(SystemConstants.BY_XPATH,
                        "//table[@test-id='assing-tbl']//input[@value='"+ ConstantReader.
                                getInstance().getProperties().get(SystemConstants.WORKF_USERNAME)+"']"));

    }

    public void confirmBtn(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//button[@test-id='modal-action-btn']"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH, "//*[@test-id='spinner-component']"));
    }

}
