package org.unicomer.pages.emma;

import net.serenitybdd.core.pages.WebElementFacade;
import org.unicomer.constants.SystemConstants;
import org.unicomer.helpers.PageCommonAction;
import org.unicomer.utils.Calculations;
import org.unicomer.utils.IdCalculator;

public class RequestStatus extends PageCommonAction {


    public void insertDocument(){

        doAction(SystemConstants.PA_SEND_KEYS,
                findElement(SystemConstants.BY_ID,
                        "inputDoc"),
                IdCalculator.getInstance().getTRN());
    }

    public void clickComboType(){
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_ID,
                        "type"));
    }

    public void selectDocType(String docType){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//mat-option//span[contains(text(),'"+docType+"')]"));
    }

    public void clickCombo(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_ID,
                        "requestType"));
    }

    public void selectOption(String opt){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//mat-option//span[contains(text(),'"+opt+"')]"));
    }

    public void clickCheck(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_ID,
                        "button-check-credit-status"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
    }

    public void clickOk(){
        try{
            isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                    "//*[contains(text(),'Loading...')]"));
            WebElementFacade wef = findElement(SystemConstants.BY_XPATH,
                    "//span[contains(text(),'Ok')]");
            takeScreenShoot();
            doAction(SystemConstants.PA_CLICK, wef);
        }
        catch(org.openqa.selenium.NoSuchElementException e){
            clickCheck();
            clickOk();
        }
    }

    public void clickPromotion(){
        try{
            doAction(SystemConstants.PA_CLICK,
                    findElement(SystemConstants.BY_XPATH,
                            "(//mat-select[@id='type'])[2]"));
        }catch(org.openqa.selenium.NoSuchElementException e){
            clickCheck();
            clickPromotion();
        }
    }

    public void clickMonth(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_ID,
                        "periodMonth"));
    }

    public void clickStore(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//*[@formcontrolname='store']"));
    }

    public void setVendor(String vendor){
        doAction(SystemConstants.PA_SEND_KEYS,
                findElement(SystemConstants.BY_XPATH,
                        "//*[@formcontrolname='storeNo']"), vendor);
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        takeScreenShoot();
    }

    public void selectStore(String opt){
        try{
            doAction(SystemConstants.PA_CLICK,
                    findElement(SystemConstants.BY_XPATH,
                    "//span[contains(text(),'"+opt+"')]"));
        }catch(org.openqa.selenium.NoSuchElementException e){
            clickStore();
            selectStore(opt);
        }

    }

    public void sendData(){
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading..')]"));
        takeScreenShoot();
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_ID,
                        "button-calculate-loan"));
        //sleepThread();
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading..')]"));
    }

    public void getLoanValues(String loan){
        WebElementFacade elementFacade =
                findElement(SystemConstants.BY_XPATH,
                        "//*[contains(text(),'"+loan+"')]");
        String loanValue = elementFacade.getText().
                replace(",","").
                replace("$","");

        Calculations.getInstance().cpiFromLoanCalculation(loanValue);
        Calculations.getInstance().cpiTaxFromLoanCalculation(loanValue);
    }

    public void getPrincipalBalanceCalc(String balance){
        WebElementFacade elementFacade =
                findElement(SystemConstants.BY_XPATH,
                        "(//*[contains(text(),'"+balance+"')])[1]");
        String loanValue = elementFacade.getText().
                replace(",","").
                replace("$","");

        Calculations.getInstance().cpiFromLoanCalculation(loanValue);
        Calculations.getInstance().cpiTaxFromLoanCalculation(loanValue);
    }

}
