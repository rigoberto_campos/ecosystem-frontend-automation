package org.unicomer.pages.emma;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.NoSuchElementException;
import org.unicomer.constants.SystemConstants;
import org.unicomer.helpers.PageCommonAction;
import org.unicomer.utils.IdCalculator;

public class HomePage extends PageCommonAction {
    WebElementFacade selectCountry, btnContinue;
    WebElementFacade btnNC;

    public void selectCountry(){
        selectCountry = findElement(SystemConstants.BY_ID, "mat-select-0");
        doAction(SystemConstants.PA_CLICK,selectCountry);
    }

    public void setCountry(String country){
         try {
             doAction(SystemConstants.PA_CLICK,
                     findElement(SystemConstants.BY_XPATH,
                             "//span[contains(text(),'"+country+"') and @class='mat-option-text']"));
             takeScreenShoot();
         }catch (org.openqa.selenium.TimeoutException | NoSuchElementException e){
             selectCountry();
             setCountry(country);
         }
    }

    public void clickContinue(){
        btnContinue = findElement(SystemConstants.BY_XPATH, "//span[contains(text(),'CONTINU')]");
        doAction(SystemConstants.PA_CLICK, btnContinue);
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        takeScreenShoot();
    }

    public void alertCountry(){
        try {
            isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                    "//*[contains(text(),'Loading...')]"));
            waitForElementVisibility(findElement(SystemConstants.BY_ID,
                    "cdk-overlay-0"));
        }
        catch (NoSuchElementException e) {
            alertCountry();
        }

    }

    public void createNewClient(){
        try {
            //waitForUrl("https://credits-uat.unicomer.com/#/");
            btnNC = findElement(SystemConstants.BY_XPATH, "(//span[contains(text(),'NEW CREDIT APPLICATION')])[2]");
            doAction(SystemConstants.PA_CLICK, btnNC);
        }catch (NoSuchElementException e){
            btnNC = findElement(SystemConstants.BY_XPATH, "(//span[contains(text(),'NEW CREDIT APPLICATION')])[2]");
            doAction(SystemConstants.PA_CLICK, btnNC);
        }
    }

    public void paymentMenu(){
        try {
            doAction(SystemConstants.PA_CLICK,
                    findElement(SystemConstants.BY_XPATH,
                            "(//button[@routerlink='payments'])[2]"));
        }catch(NoSuchElementException e){
            doAction(SystemConstants.PA_CLICK,
                    findElement(SystemConstants.BY_XPATH,
                            "(//button[@routerlink='payments'])[2]"));
        }
    }

    public void clickLoan(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "(//span[contains(text(),' Loan quoter ')])[2]"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
    }

    public void searchTrnValidation(){
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                "//input[@id='inputDoc']"), IdCalculator.getInstance().getTRN());
    }

    public void clickSearchTrn(){
        try {
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                    "//span[contains(text(),' Search ')]"));
        }catch(NoSuchElementException e){
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                    "//span[contains(text(),'CHECK')]"));
        }
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        takeScreenShoot();

    }

    public void waitApproveBox(){
        try {
            //waitForElementVisibility(findElement(SystemConstants.BY_XPATH, "//*[contains(text(),'Cancel')]"));
            isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                    "//*[contains(text(),'Loading...')]"));
            takeScreenShoot();
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                    "//*[contains(text(),'Cancel')]"));
        }catch(NoSuchElementException e){

        }
    }

    public void clickVerifyStatusMenu(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "(//span[contains(text(),'Credit application status')])[2]"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
    }

    public void goToMain() {
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//img[@alt='Emma_Logo']"));
    }

    public void selectDocumentType() {
        try {
            isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                    "//*[contains(text(),'Loading...')]"));
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "//mat-select[contains(@id, 'mat-select-')]"));
        }catch (org.openqa.selenium.TimeoutException | NoSuchElementException toe){
            selectDocumentType();
        }

    }

    public void setDocumentType(String opt){
        try {
            if (opt.equals("Cedula de identidad"))
                doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,"//mat-option//span[contains(text(),'Cédula de Identidad')]"));
            else
                doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,"//mat-option//span[contains(text(),'"+opt+"')]"));
            takeScreenShoot();
        }catch (org.openqa.selenium.TimeoutException | NoSuchElementException toe){
            selectDocumentType();
            setDocumentType(opt);
        }

    }

    public void insertDocumentId(String opt){
        doAction(SystemConstants.PA_SEND_KEYS,
                findElement(SystemConstants.BY_XPATH,"//*[@formcontrolname='inputDoc']"), opt);
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
    }
    public void searchClient() {
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//span[contains(text(),' Buscar cliente ')]"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
    }

    public void buttonComenzarAplicacion() {
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//button/span[contains(text(),' COMENZAR APLICACIÓN ')]"));
    }
}
