package org.unicomer.pages.emma;

import org.unicomer.constants.SystemConstants;
import org.unicomer.helpers.PageCommonAction;

public class PaymentPage extends PageCommonAction {

    public void selectDocumentType(){
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//mat-select[@id='type']"));
    }

    public void chooseDocumentType(String document){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//mat-option//span[contains(text(),'"+document+"')]"));

    }

    public void insertDocument(String document){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_ID,
                "inputDoc"),
                document);
    }

    public void checkDocument(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//span[contains(text(),'CHECK CLIENT')]"));

    }

    public void continueRequest(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_ID,
                "button-check-client-payments"));
    }

    public void shoppingCart(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//button//mat-icon[contains(text(),'shopping_cart')]"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
    }

    public void closeReviewBox(){
        takeScreenShoot();
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//span[contains(text(),'CLOSE')]"));
    }

    public void clickToSelectType(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_ID,
                "type"));
    }

    public void selectTypeOption(String hpOption){
        doAction(SystemConstants.BY_ID,
                findElement(SystemConstants.BY_XPATH,
                        "//mat-option//span[contains(text(),'"+hpOption+"')]"));
    }

    public void clickToSelectPeriod(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_ID,"periodMonth"));
    }

    public void selectPeriod(String month){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//div[@id='periodMonth-panel']//span[text()=' "+month+" ']"));
    }

    public void checkOut(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//button//span[contains(text(),'CHECK OUT')]"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        takeScreenShoot();
    }

}
