package org.unicomer.pages.emma;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.unicomer.constants.SystemConstants;
import org.unicomer.helpers.PageCommonAction;
import org.unicomer.utils.IdCalculator;

public class NewClientPage extends PageCommonAction {

    WebElementFacade inputTrn, inputName;

    public void insertTRN(){
        inputTrn = findElement(SystemConstants.BY_ID, "inputDoc");
        doAction(SystemConstants.PA_SEND_KEYS, inputTrn, IdCalculator.getInstance().getTRN());
        if(!inputTrn.getAttribute("value").contains(IdCalculator.getInstance().getTRN())){
            insertTRN();
        }
        takeScreenShoot();
    }

    public void sendTRN(){
        try {
            takeScreenShoot();
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_ID, "button-form-credit-validation"));
            isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                    "//*[contains(text(),'Loading...')]"));
        }catch (org.openqa.selenium.TimeoutException e) {
            insertTRN();
            sendTRN();
        }
    }

    public void firsName(){
        try {

            inputName = findElement(SystemConstants.BY_XPATH, "//input[@formcontrolname='firstName']");
            isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                    "//*[contains(text(),'Loading...')]"));
            doAction(SystemConstants.PA_SEND_KEYS, inputName,"Rigoberto");

            if(!inputName.getAttribute("value").contains("Rigoberto")){
                firsName();
            }
        }catch (NoSuchElementException e){
            firsName();
        }

    }

    public void middleName(){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                "//input[@formcontrolname='middleName']"),"Berto");
    }

    public void lastName(){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                "//input[@formcontrolname='lastName']"),"Campos");
    }

    public void secondLastName(){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                "//input[@formcontrolname='secondLastName']"),"Vega");
    }

    public void phoneNumber(){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                "//input[@formcontrolname='phone']"),"112233444");
    }

    public void email(){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                "//input[@formcontrolname='email']"),"m"+IdCalculator.getInstance().getTRN() + "@mail.com");
    }

    public void birthDate(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//input[@formcontrolname='dateOfBirth']"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"(//*[contains(@aria-describedby,'mat-calendar-button-')])[1]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"(//div[text()=' 1990 '])[1]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"(//div[contains(text(),' JAN')])[1]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"(//div[text()=' 1 '])[1]"));

    }

    public void sendData(){
        takeScreenShoot();
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_ID,"button-form-credit-evaluate"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
    }

    public void successFully(){
        try {
            isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                    "//*[contains(text(),'Loading...')]"));
            takeScreenShoot();
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_ID, "preApprovedOk"));
        } catch (org.openqa.selenium.TimeoutException e) {
            sendData();
        }
    }

    public void selectGender(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[1]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'Male')]"));
    }
    public void selectCountryOrigin(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[2]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'Jamaica')]"));
    }
    public void selectMarriedStatus(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[3]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'Married')]"));
    }
    public void selectPaymentFreq(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[4]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"(//*[contains(text(),' Monthly ')])[2]"));
    }

    public void numberDependents(){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH, "(//input)[1]"), "2");
    }

    public void netIncome(){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH, "(//input)[2]"), "770000");
    }

    public void homeAddress(){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH, "//textarea"),
                "Address Address Address Address Address 10");
    }

    public void submitInfo(){
        takeScreenShoot();
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_ID, "button-form-credit-qualification"));

    }

    public void confirmAll(){
        takeScreenShoot();
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//span[contains(text(),'CONFIRM INCOME')]"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
    }

    public void selectOccupation(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[1]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'Business O')]"));
    }

    public void monthsWorkIn(){
        try {
            doAction(SystemConstants.PA_SEND_KEYS,findElement(SystemConstants.BY_XPATH,"(//input)[4]"),"120");
        }catch (InvalidElementStateException e){
            doAction(SystemConstants.PA_SEND_KEYS,findElement(SystemConstants.BY_XPATH,"(//input)[1]"),"120");
        }

    }

    public void selectEconomyAct(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[2]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'Salary')]"));
    }

    public void selectDateWork(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//input[@aria-haspopup='dialog']"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//span[contains(@id,'mat-calendar-button-')]"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,"//div[contains(text(),'2010')]"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,"//div[contains(text(),'JAN')]"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,"//div[contains(text(),' 1 ')]"));
    }

    public void selectPP(String politicalPerson){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[3]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[text()=' "+politicalPerson+" ']"));
    }

    public void selectParish(String region){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[1]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'"+region+"')]"));
    }

    public void  employerName(){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                        "(//input)[2]"),
                    "Rigo Berto Cam");
    }

    public void workAddress(){
        doAction(SystemConstants.PA_SEND_KEYS,
                findElement(SystemConstants.BY_XPATH,
                        "(//textarea)[1]"),
                IdCalculator.getInstance().getTRN());
    }

    public void  workNumber(){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                        "(//input)[3]"),
                IdCalculator.getInstance().getTRN());
    }

    public void selectDistrict(String region){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[2]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'"+region+"')]"));
    }

    public void selectResidentStatus(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[3]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'Owner')]"));
    }

    public void currentAddress(){
        doAction(SystemConstants.PA_SEND_KEYS,findElement(SystemConstants.BY_XPATH,"(//input)[1]"),"122");
    }

    public void contactName(String nroInput){
        doAction(SystemConstants.PA_SEND_KEYS,findElement(SystemConstants.BY_XPATH,"(//input)["+nroInput+"]"),"Rigoberto Campos");
    }

    public void contactNameAruba(){
        doAction(SystemConstants.PA_SEND_KEYS,findElement(SystemConstants.BY_XPATH,"(//input)[3]"),"Rigoberto Campos V");
    }

    public void contactNumber(String nroInput){
        doAction(SystemConstants.PA_SEND_KEYS,findElement(SystemConstants.BY_XPATH,"(//input)["+nroInput+"]"),"8122334");
    }

    public void contactNumberAruba(){
        doAction(SystemConstants.PA_SEND_KEYS,findElement(SystemConstants.BY_XPATH,"(//input)[4]"),"7122334");
    }

    public void uploadFile(String optFile){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,"//span[contains(text(),'"+optFile+"')]"));
        WebElement chooseFile = getCommonDriver().findElement(By.xpath("//input[contains(@type,'file')]"));
        chooseFile.sendKeys("C:\\Users\\rigoberto_campos\\OneDrive - Grupo Unicomer Unicomer Group\\Imágenes\\dni.PNG");
        //waitForElementVisibility(findElement(SystemConstants.BY_XPATH,"//span[contains(text(),'Upload Document')]"));
        //doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//span[contains(text(),'Upload Document')]"));
    }

    public void sendAllData(){
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//span[contains(text(),'NEXT')]"));
    }

    public void waitingResponse(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_ID, "button-credit-app-waiting-response"));
    }

    public void typeoflinePY(String typeofline){
        try {
            isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                    "//*[contains(text(),'Loading...')]"));
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[1]"));
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "//*[contains(text(),' " + typeofline + " ')]"));
        }catch (org.openqa.selenium.TimeoutException | NoSuchElementException toe){
            typeoflinePY(typeofline);
        }
    }

    public void firstNamePY(String firstname){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH, "(//input)[3]"),firstname);
    }

    public void firstlastNamePY(String firstlastname){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                "(//input)[6]"),firstlastname);
    }

    public void selectGenderPY(String gender){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[2]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),' "+gender+" ')]"));
    }

    public void selectMarriedStatusPY(String marriedstatus){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH, "(//mat-select)[3]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),' "+marriedstatus+" ')]"));
    }

    public void phoneNumberPY(String phonenumber){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                "(//input)[10]"),phonenumber);
    }

    public void emailPY(){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_ID,
                "mat-input-36"),"m"+IdCalculator.getInstance().getTRN() + "@mail.com");
    }

    public void datePY(String anio, String mes, String dia){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//*[contains(@class,'mat-datepicker-toggle')]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,
                "//mat-calendar/mat-calendar-header/div/div/button[1]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"(//div[text()=' "+anio+" '])[1]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"(//div[contains(text(),'"+mes+"')])[1]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"(//div[text()=' "+dia+" '])[1]"));

    }
    public void sourceofIncomePY(String sourceofIncome) {
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "(//mat-select)[5]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),' "+sourceofIncome+" ')]"));
    }
    public void paymentFrequencyPY(String paymentFrequency) {
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "(//mat-select)[6]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),' "+paymentFrequency+" ')]"));
    }

    public void setIncomePY(String income){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                "(//input)[12]"), income);
    }
    public void selectDeparmentPY(String deparment, String num){

        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "(//mat-select)["+num+"]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),' "+deparment+" ')]"));
    }

    public void selectDistrictPY(String district, String num){
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "(//mat-select)["+num+"]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),' "+district+" ')]"));

    }
    public void selectNeighborhoodPY(String neighborhood, String num){
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "(//mat-select)["+num+"]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),' "+neighborhood+" ')]"));
    }
    public  void uploadImage(String opt) {
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));

        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//span[contains(text(),'"+opt+"')]"));

        //Driver hisBrowser = getDriver();

        WebElement chooseFile = getCommonDriver().findElement(By.xpath("//input[contains(@type,'file')]"));
        chooseFile.sendKeys("C:\\Users\\rigoberto_campos\\OneDrive - Grupo Unicomer Unicomer Group\\Imágenes\\dni.PNG");
        /*isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        waitForElementVisibility(findElement(SystemConstants.BY_XPATH,"//*[@class='uploadButton']/label/a"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//*[@class='uploadButton']/label/a"));*/



    }
    public void sendDataPY() {
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_ID,
                "button-form-credit-evaluate"));

    }

    public  void preApprovedOk() {
        try {
            isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                    "//*[contains(text(),'Loading...')]"));
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                    "//*[contains(@id,'preApprovedOk')]"));

        }catch (org.openqa.selenium.NoSuchElementException e){
            preApprovedOk();
        }
    }

    public  void selectCountryOfBirthPY(String country) {
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        try {
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//*[@formgroupname='personalForm']/div[2]/div/ng-select"));
            String  countryOfB = country.toUpperCase();
            doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'"+countryOfB+"')]"));
        }catch (NoSuchElementException e){
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                    "//*[@formgroupname='personalForm']/div[1]/div/ng-select"));
            doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'"+country+"')]"));
        }

    }
    public void setInputPY(String value, String input){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                "(//input)["+input+"]"), value);
        takeScreenShoot();
    }
    public void setTextAreaPY(String value, String input){
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                "(//textarea)["+input+"]"), value);
    }
    public  void selectPhoneTypePY(String formType, String selectValue, String phoneType2, String selectArea, String area, String inputFijo, String inputMovil, String number) {
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "(//mat-select)["+selectValue+"]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),' "+phoneType2+" ')]"));
        if (phoneType2.contains("fijo")) {
            isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                    "//*[contains(text(),'Loading...')]"));
            doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                    "//*[@formgroupname='"+formType+"']/div["+selectArea+"]/div/ng-select"));
            if (formType.equals("contactForm")) {
                doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,
                        "//*[@formgroupname='contactForm']/div["+selectArea+"]/div/ng-select/ng-dropdown-panel/div/div/div/span[text()='"+area+"']"));

            } else{
                doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//span[text()='"+area+"']"));
            }
            isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                    "//*[contains(text(),'Loading...')]"));
            doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                    "(//input)["+inputFijo+"]"), number);
        }
        else {
            doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_XPATH,
                    "(//input)["+inputMovil+"]"), number);
        }
    }
    public void sendDataStepPY(String id) {
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_ID,
                id));

    }
    public void selectPY (String value, String nro) {
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "(//mat-select)["+nro+"]"));

        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'"+value+"')]"));
    }
    public void selectOcupationPY (String value) {
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//*[@formgroupname='ocupationForm']/div[3]/div/ng-select"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'"+value+"')]"));
    }
    public void firstDayHome(String anio, String mes){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//*[contains(@class,'mat-datepicker-toggle')]"));

        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"(//div[text()=' "+anio+" '])[1]"));
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,"(//div[contains(text(),'"+mes+"')])[1]"));
        takeScreenShoot();
    }

    public void windowsFinal() {
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        waitForElementVisibility(findElement(SystemConstants.BY_XPATH,"//*[contains(text(),'REGRESAR')]"));
        takeScreenShoot();
        doAction(SystemConstants.PA_CLICK,findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'REGRESAR')]"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
    }

}
