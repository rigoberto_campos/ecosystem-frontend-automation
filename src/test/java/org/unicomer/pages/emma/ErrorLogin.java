package org.unicomer.pages.emma;

import net.serenitybdd.core.pages.WebElementFacade;
import org.unicomer.constants.SystemConstants;
import org.unicomer.helpers.PageCommonAction;

public class ErrorLogin extends PageCommonAction {

    private WebElementFacade restartLogin(){
        return
                findElement(SystemConstants.BY_ID, "id='loginRestartLink'");
    }

    private WebElementFacade continueLogin(){
        return
                findElement(SystemConstants.BY_ID, "id='loginContinueLink'");
    }

    public void clickContinue(){
        doAction(SystemConstants.PA_CLICK, continueLogin());
    }

    public void clickRestart(){
        doAction(SystemConstants.PA_CLICK, restartLogin());
    }

    public void validateElement(){

    }

}
