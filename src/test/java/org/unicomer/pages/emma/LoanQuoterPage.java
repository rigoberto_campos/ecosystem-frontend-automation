package org.unicomer.pages.emma;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.unicomer.constants.SystemConstants;
import org.unicomer.helpers.PageCommonAction;

public class LoanQuoterPage extends PageCommonAction {

    public void insertAmount(){
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        doAction(SystemConstants.PA_SEND_KEYS, findElement(SystemConstants.BY_ID,
                "amount"), "65000");
    }

    public void noCpi(){
        //doAction(SystemConstants.PA_CLICK, );
        JavascriptExecutor executor = (JavascriptExecutor)getDriver();
        executor.executeScript("arguments[0].click();",
                (WebElement) findElement(SystemConstants.BY_XPATH,
                        "(//input[@type='radio'])[1]"));
    }

    public void clickToCheck(){
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_XPATH,
                "//span[contains(text(),'CHECK CLIENT')]"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
    }

    public void generateLoan(){
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
        takeScreenShoot();
        doAction(SystemConstants.PA_CLICK, findElement(SystemConstants.BY_ID, "button-calculate-loan"));
        isElementOnScreen(findElement(SystemConstants.BY_XPATH,
                "//*[contains(text(),'Loading...')]"));
    }

    public void waitLoanAlert(){
        waitElementOnScreen(findElement(SystemConstants.BY_XPATH,"//app-alert-credit-status"));
        takeScreenShoot();
    }

    public void selectDocumentType(){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_ID,
                        "type"));
    }

    public void selectDocumentType(String document){
        doAction(SystemConstants.PA_CLICK,
                findElement(SystemConstants.BY_XPATH,
                        "//mat-option//span[contains(text(),'"+document+"')]"));
    }

}
