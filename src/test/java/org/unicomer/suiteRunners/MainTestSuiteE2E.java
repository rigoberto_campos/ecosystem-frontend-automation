package org.unicomer.suiteRunners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/PY/new_client.feature",
        tags = "@e2e"
)
public class MainTestSuiteE2E {

}
