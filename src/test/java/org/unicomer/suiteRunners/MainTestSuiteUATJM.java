package org.unicomer.suiteRunners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/JM/CL/cashloan_newclient.feature",
        tags = "@Jm and @Cashloan and @OnlyClient"
)
public class MainTestSuiteUATJM {

}
