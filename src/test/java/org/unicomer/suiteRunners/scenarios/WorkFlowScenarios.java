package org.unicomer.suiteRunners.scenarios;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebDriver;
import org.unicomer.helpers.DriverManager;
import org.unicomer.steps.workFlow.LoginPageStep;
import org.unicomer.steps.workFlow.RequestTrackingStep;
import org.unicomer.steps.workFlow.TimeMonitoringStep;
import org.unicomer.utils.documents.DocumentManager;
import org.unicomer.utils.IdCalculator;

public class WorkFlowScenarios {
    @Steps
    LoginPageStep loginWF;

    @Steps
    org.unicomer.steps.workFlow.HomePageStep homePageWF;

    @Steps
    RequestTrackingStep request;

    @Steps
    TimeMonitoringStep monitoring;

    @When("Login into WorkFlow")
    public void loginWorkFlow(){
        loginWF.navigateWF();
        loginWF.setCredentials();
    }

    @Then("Select {string} country in WorkFlow")
    public void countryWorkFlow(String country){
        homePageWF.selectCountry(country);
        homePageWF.selectLanguage();
    }

    @Then("Select {string} menu in WorkFlow")
    public void menuWorkFlow(String menu){
        homePageWF.selectMenu(menu);
    }

    @Then("Select {string} menu and assign request in WorkFlow")
    public void assignRequest(String menu){
        String document =  IdCalculator.getInstance().getTRN();
        DocumentManager.getInstance().setTitle(document);
        homePageWF.selectMenu(menu);
        monitoring.allin(document);
    }

    @Then("Select {string} menu and assign request in WorkFlow {string}")
    public void assignRequest(String menu, String document){
        IdCalculator.getInstance().setTRN(document);
        DocumentManager.getInstance().setTitle(document);
        homePageWF.selectMenu(menu);
        monitoring.allin(document);
    }

    @Then("Approved client request with document in WorkFlow")
    public void searchRequest(){
        String document =  IdCalculator.getInstance().getTRN();
        DocumentManager.getInstance().setTitle(document + "-WORKFLOW-ASSIGNANDVALIDATE");
        request.searchByTRN(document);
        request.dataValidationClient();
        request.confirmValidationClient();
        request.approvedClient();
    }

    @Then("Approved client request with document {string} in WorkFlow")
    public void searchRequest(String document){
        request.searchByTRN(document);
        request.dataValidationClient();
        request.confirmValidationClient();
        request.approvedClient();
    }

    @Then("Approved client request with document {string} in WorkFlow Aruba")
    public void searchRequestAruba(String document){
        IdCalculator.getInstance().setTRN(document);
        DocumentManager.getInstance().setTitle(document);
        request.searchByTRN(document);
        request.confirmValidationClient();
        request.approvedClient();
    }

    @Then("Approved client request with document in WorkFlow Aruba")
    public void searchRequestAruba(){
        String document = IdCalculator.getInstance().getTRN();
        DocumentManager.getInstance().setTitle(document);
        request.searchByTRN(document);
        request.confirmValidationClient();
        request.approvedClient();
    }

    @Then("Validate client disbursement in WorkFlow")
    public void reviewClientDisbursement(){
        String id = IdCalculator.getInstance().getTRN();
        request.accountAllRequest(id);
        request.clickRequestNumber();

    }
}
