package org.unicomer.suiteRunners.scenarios;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.unicomer.steps.mambu.ClientPageStep;
import org.unicomer.steps.mambu.LoginPageStep;
import org.unicomer.steps.mambu.MainPageStep;
import org.unicomer.utils.documents.DocumentManager;
import org.unicomer.utils.IdCalculator;

public class MambuScenarios {

    @Steps
    private LoginPageStep login;

    @Steps
    private MainPageStep main;

    @Steps
    private ClientPageStep client;

    @When("Open and login in Mambu web app")
    public void navigateMambu(){
        login.navigate();
        login.login();
    }

    @Then("Looking for ID")
    public void searchId(){
        String id = IdCalculator.getInstance().getTRN();
        DocumentManager.getInstance().setTitle(id);
        main.searchId(id);
        main.selectCashLoanAccount();
    }

    @Then("Looking for ID {string}")
    public void searchId(String id){
        IdCalculator.getInstance().setTRN(id);
        DocumentManager.getInstance().setTitle(id);
        main.searchId(id);
    }

    public void generateDocument(){
        DocumentManager.
                getInstance().
                setTitle(IdCalculator.
                        getInstance().
                        getTRN());
        DocumentManager.getInstance().
                createNormalDocument(DocumentManager.getInstance().getTitle());
    }

    @Then("Details schedule and transaction from selected account")
    public  void selectMenu(){
        client.detailSubMenu();
        client.scheduleSubMenu();
        client.transactionSubMenu();
        //client.validateRepayment();
        generateDocument();
        //client.getTypeChannel();
    }

    @Then("Pay account")
    public void paidAccount(){
        String amount = DocumentManager.getInstance().getAccountValue("Total Balance");
        client.clickRePayment(amount);
    }

    @Then("Pay account {string}")
    public void paidAccount(String amount){
        client.clickRePayment(amount);
    }

    @Then("Pay Off amount {string} and day {string}")
    public void paidOffAccount(String amount, String day){
        client.payBox(amount);
        client.selectBackDate();
        client.selectDay(day);
        client.paymentApply();
    }

    @Given("{string} as payment method")
    public void setPaymentMethod(String method){
        //DocumentManager.getInstance().setValueChannel(method);
    }
}

