package org.unicomer.suiteRunners.scenarios;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.unicomer.steps.emma.*;
import org.unicomer.utils.ConstantReader;
import org.unicomer.utils.IdCalculator;

public class EmmaScenarios {

    @Steps
    org.unicomer.steps.emma.LoginPageStep loginPage;

    @Steps
    HomePageStep homePage;

    @Steps
    NewClientPageStep newClientPage;

    @Steps
    LoanQuoterStep loan;

    @Steps
    RequestStatusStep request;

    @Steps
    ErrorLoginSteps errorLogin;

    @Steps
    PaymentSteps payment;

    @When("Login into Emma")
    public void loginEmma(){
        loginPage.openEmma();
        loginPage.loginEmma();
        //loginPage.confirmEmma();
    }

    @Then("Select {string} country in Emma")
    public void selectCountry(String country){
        homePage.selectCountry(country);
    }

    @Then("Create new client in Emma")
    public void newClient(){
        homePage.createNewClient();
    }

    @Then("Complete client personal information when client is pp {string} in Emma")
    public void newClientInfo(String political){
        newClientPage.insertTRN();
        newClientPage.personalInfo();
        newClientPage.personalInfoPayment();
        newClientPage.personalWorkInfo(political);
    }

    @Then("Complete client region information from {string} {string} {string} in Emma")
    public void regionInformation(String country, String region1, String region2){
        newClientPage.geographicInfo(region1, region2);
        newClientPage.contactInfo(country);
        newClientPage.filesInfo();
    }

    @Then("Verify client is approved in Emma")
    public void verifyTrn(){
        String document = IdCalculator.getInstance().getTRN();
        homePage.verifyTrn();
    }

    @Then("Search client status from {string} in Emma")
    public void clientStatus(String country){
        homePage.searchClientStatus(country);
    }

    @Then("Go to main page in Emma")
    public void goToMain(){
        homePage.goToMain();
    }

    @Then("Loan quoter with client TRN in Emma")
    public void loanProcess(){
        homePage.clickLoan();
        loan.validateLoan();
        loan.calculateQuote();
    }

    @Then("Loan quoter with client {string} document in Emma")
    public void loanProcessAruba(String document){
        homePage.clickLoan();
        loan.loanQuoterAruba(document);
        loan.calculateQuoteAruba();
    }

    @Then("Go to credit status menu in Emma")
    public void creditStatusMenu(){
        homePage.clickVerifyMenu();
    }

    @Then("Select request parameters {string} document id and {string} as type in Emma")
    public void requestParameter(String doc, String type){
        String id = IdCalculator.getInstance().getTRN();
        request.requestParams(type, doc);
    }

    @Then("Set loan params {string} as promotion with {string} months {string} as store and {string} as vendor in Emma")
    public void loanParams(String promotion, String month, String store, String vendor){
        request.loanParams(promotion, month, store, vendor);
        request.sendData();
    }

    @Then("Close message for document ready in Emma")
    public void docReady(){
        request.documentsReady();
    }

    @Given("Evidence client with {string} as document_id and {string} as document_type in Emma")
    public void setDocumentId(String id, String type){
        IdCalculator.getInstance().setTRN(id);
    }

    @Then("Go to payment menu in Emma")
    public void paymentMenu(){
        homePage.paymentMenu();
    }

    @Then("Select {string} as document type in Emma")
    public void SelectDocument(String document){
        payment.selectDocumentType();
        payment.chooseDocumentType(document);
    }

    @Then("Insert {string} as document in Emma")
    public void insertDocument(String document){
        payment.insertDocument(document);
        payment.checkDocument();
        payment.continueRequest();
    }

    @Then("Complete hp information with {string} as hp type and {string} as period time in Emma")
    public void hpValidation(String hpOption, String month) {
        payment.shoppingCart();
        payment.closeReviewBox();
        payment.clickToSelectType();
        payment.selectTypeOption(hpOption);
        payment.clickToSelectPeriod();
        payment.selectPeriod(month);
        payment.checkOut();
    }

    @Then("Search client Paraguay Document Type: {string} and Document Id: {string}")
    public void searchClientPY(String type, String doc) {
        homePage.searchDocumentPY(type, doc);
    }
    @Then("Evaluation Personal Information {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}")
    public void creditEvaluationPersonalInformation(String typeofline, String firstname, String firstlastname,
                                                    String gender, String marriedstatus, String anio, String mes, String dia,
                                                    String phonenumber, String sourceofIncome, String paymentFrequency,
                                                    String income, String deparment, String district, String neighborhood) {
        newClientPage.creditEvaluation(typeofline, firstname, firstlastname,
                gender, marriedstatus, anio, mes, dia,
                phonenumber, sourceofIncome, paymentFrequency,
                income, deparment, district, neighborhood);
    }
    @Then("Credit questions Step 1 of 5 {string}, {string}, {string}, {string}, {string}")
    public void creditQuestions1(String country, String dependents, String phonetype2, String area2, String number2){
        newClientPage.creditQuestions1(country,dependents, phonetype2, area2, number2);
    }
    @Then("Credit questions Step 1 of 5 e2e {string}, {string}, {string}, {string}, {string}")
    public void creditQuestions1e2e(String country, String dependents, String phonetype2, String area2, String number2){
        newClientPage.creditQuestions1e2e(country,dependents, phonetype2, area2, number2);
    }
    @Then("Credit questions Step 2 of 5 {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}")
    public void creditQuestions2(String company, String year, String month, String day, String ocupation, String departament,
                                 String district, String barrio, String calle, String referencia,
                                 String phoneTypeWork1, String phoneAreaWork1, String phoneNumberWork1,
                                 String phoneTypeWork2, String phoneAreaWork2, String phoneNumberWork2){
        newClientPage.creditQuestions2(company,  year,  month,  day,  ocupation,  departament,
                district,  barrio,  calle,  referencia,
                phoneTypeWork1,  phoneAreaWork1,  phoneNumberWork1,
                phoneTypeWork2,  phoneAreaWork2,  phoneNumberWork2);
    }
    @Then("Credit questions Step 3 of 5 {string}, {string}, {string}, {string}, {string}")
    public void creditQuestions3(String calleResidencia, String refResidencia, String anio, String mes, String tipo){
        newClientPage.creditQuestions3( calleResidencia,  refResidencia,  anio,  mes,  tipo);
    }
    @Then("Credit questions Step 4 of 5 {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}")
    public void creditQuestions4(String refContacto1, String parentesco1, String tipoTelefono1, String area1, String num1,
                                 String refContacto2, String parentesco2, String tipoTelefono2, String area2, String num2,
                                 String refContacto3, String parentesco3, String tipoTelefono3, String area3, String num3){
        newClientPage.creditQuestions4( refContacto1,  parentesco1,  tipoTelefono1, area1, num1,
                refContacto2,  parentesco2,  tipoTelefono2, area2, num2,
                refContacto3,  parentesco3,  tipoTelefono3, area3, num3);
    }
    @Then("Credit questions Step 5 of 5 {string}")
    public void creditQuestions4(String typeofline) {
        newClientPage.creditQuestions5(typeofline);
    }

    @Given("Env {string}")
    public void setEnv(String env){
        String file = env;
        ConstantReader.getInstance().readFile(file);
        IdCalculator.getInstance();
    }
}
