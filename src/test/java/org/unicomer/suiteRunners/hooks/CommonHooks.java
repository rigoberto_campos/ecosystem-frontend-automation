package org.unicomer.suiteRunners.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import org.unicomer.helpers.DriverManager;
import org.unicomer.utils.IdCalculator;
import org.unicomer.utils.documents.DocumentManager;
import org.unicomer.utils.documents.ExcelDocument;

public class CommonHooks {

    @Managed
    WebDriver driver;

    @Before
    public void boomDriver(){
        driver = DriverManager.getInstance().getDriver();
    }

    @After("@ExcelDocument")
    public void generateExcel(){
        ExcelDocument excel =  DocumentManager.
                getInstance().
                createXlsxDoc(DocumentManager.getInstance().getTitle());
        DocumentManager.getInstance().addDataToExcel(excel, 0,1);
        DocumentManager.getInstance().saveExcelDoc(excel);
    }

    @After("@Document")
    public void generateDocument(){
        DocumentManager.
                getInstance().
                setTitle(IdCalculator.
                        getInstance().
                        getTRN());
        DocumentManager.getInstance().
                createNormalDocument(DocumentManager.getInstance().getTitle());
    }
}
