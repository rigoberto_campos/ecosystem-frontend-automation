package org.unicomer.steps.emma;

import net.thucydides.core.annotations.Step;
import org.unicomer.pages.emma.RequestStatus;

public class RequestStatusStep {


    RequestStatus request;

    @Step("Set request params")
    public void requestParams(String requestType, String docType){
        request.clickComboType();
        request.selectDocType(docType);
        request.insertDocument();
        request.clickCombo();
        request.selectOption(requestType);
        request.clickCheck();
    }

    @Step("Loan params")
    public  void loanParams(String promotion, String month, String store, String vendor){
        request.clickPromotion();
        request.selectOption(promotion);
        request.clickMonth();
        request.selectOption(month);
        request.getLoanValues("65,000.00");
        request.getLoanValues("3,061.50");
        request.getLoanValues("459.22");
        request.clickStore();
        request.selectStore(store);
        request.setVendor(vendor);
    }

    @Step("Send params")
    public void sendData(){
        request.sendData();
    }

    public void documentsReady(){
        request.clickOk();
    }


}
