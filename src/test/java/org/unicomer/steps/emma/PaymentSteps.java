package org.unicomer.steps.emma;

import net.thucydides.core.annotations.Step;
import org.unicomer.pages.emma.PaymentPage;

public class PaymentSteps {

    PaymentPage payment;

    @Step("Click to select document type")
    public void selectDocumentType(){
        payment.selectDocumentType();
    }

    @Step("Insert document")
    public void insertDocument(String document){
        payment.insertDocument(document);
    }

    @Step("Choose document type")
    public void chooseDocumentType(String document){
        payment.chooseDocumentType(document);

    }

    @Step("Check if client has pending hp")
    public void checkDocument(){
        payment.checkDocument();
    }

    @Step("Click continue button")
    public void continueRequest(){
        payment.continueRequest();
    }

    @Step("Click in shoppingCart to review HP")
    public void shoppingCart(){
        payment.shoppingCart();
    }

    @Step("Close review box")
    public void closeReviewBox(){
        payment.closeReviewBox();
    }

    @Step("Click to select transaction type")
    public void clickToSelectType(){
        payment.clickToSelectType();
    }

    @Step("Choose transaction type")
    public void selectTypeOption(String hpOption){
        payment.selectTypeOption(hpOption);
    }

    @Step("Click to select period")
    public void clickToSelectPeriod(){
        payment.clickToSelectPeriod();
    }

    @Step("Choose period")
    public void selectPeriod(String month){
        payment.selectPeriod(month);
    }

    @Step("Send HP to review")
    public void checkOut(){
        payment.checkOut();
    }

}
