package org.unicomer.steps.emma;

import net.thucydides.core.annotations.Step;
import org.unicomer.pages.emma.NewClientPage;

public class NewClientPageStep {

    NewClientPage newClientPage;

    @Step("Send TRN Number")
    public void insertTRN(){
        newClientPage.insertTRN();
        newClientPage.sendTRN();
    }

    @Step("Personal info")
    public void personalInfo(){
        newClientPage.firsName();
        newClientPage.middleName();
        newClientPage.lastName();
        newClientPage.secondLastName();
        newClientPage.phoneNumber();
        newClientPage.email();
        newClientPage.birthDate();
        newClientPage.sendData();
        newClientPage.successFully();
    }

    @Step("Personal info payment")
    public void personalInfoPayment(){
        newClientPage.selectGender();
        newClientPage.selectCountryOrigin();
        newClientPage.numberDependents();
        newClientPage.selectMarriedStatus();
        newClientPage.selectPaymentFreq();
        newClientPage.netIncome();
        newClientPage.homeAddress();
        newClientPage.submitInfo();
        newClientPage.confirmAll();
    }

    @Step("Personal work info")
    public void personalWorkInfo(String political){
        newClientPage.selectOccupation();
        newClientPage.monthsWorkIn();
        newClientPage.selectEconomyAct();
        newClientPage.selectDateWork();
        newClientPage.employerName();
        newClientPage.workAddress();
        newClientPage.workNumber();
        newClientPage.selectPP(political);
        newClientPage.submitInfo();
    }

    @Step("Geographic info")
    public void geographicInfo(String region1, String region2){
        newClientPage.selectParish(region1);
        newClientPage.selectDistrict(region2);
        newClientPage.selectResidentStatus();
        newClientPage.currentAddress();
        newClientPage.submitInfo();
    }

    @Step("Contact info")
    public void contactInfo(String country){
        newClientPage.contactName("1");
        newClientPage.contactNumber("2");
        newClientPage.contactName("3");
        newClientPage.contactNumber("4");
        if(country.equals("Aruba")){
            newClientPage.contactNameAruba();
            newClientPage.contactNumberAruba();
        }
        newClientPage.submitInfo();
    }

    @Step("Upload files")
    public void filesInfo(){
        newClientPage.uploadFile("Front of your ID");
        newClientPage.uploadFile("Back of your ID");
        newClientPage.uploadFile("Most recent Utility Bill");
        newClientPage.uploadFile("Enter proof of your income");
        newClientPage.sendAllData();
        newClientPage.waitingResponse();
    }
    @Step("Informacion personal para la evaluacion del credito")
    public void creditEvaluation(String typeofline, String firstname, String firstlastname,
                                 String gender, String marriedstatus, String anio, String mes, String dia,
                                 String phonenumber, String sourceofIncome, String paymentFrequency,
                                 String income, String deparment, String district, String neighborhood) {
        newClientPage.typeoflinePY(typeofline);
        newClientPage.firstNamePY(firstname);
        newClientPage.firstlastNamePY(firstlastname);
        newClientPage.selectGenderPY(gender);
        newClientPage.selectMarriedStatusPY(marriedstatus);
        newClientPage.datePY(anio, mes, dia);
        newClientPage.phoneNumberPY(phonenumber);
        newClientPage.sourceofIncomePY(sourceofIncome);
        newClientPage.paymentFrequencyPY(paymentFrequency);
        newClientPage.setIncomePY(income);
        newClientPage.selectDeparmentPY(deparment,"7");
        newClientPage.selectDistrictPY(district, "8");
        newClientPage.selectNeighborhoodPY(neighborhood, "9");
        newClientPage.uploadImage("Comprobante de aceptación de investigación en burós");
        newClientPage.sendDataPY();
        newClientPage.preApprovedOk();
    }
    @Step("Preguntas para la evaluacion del credito step 1")
    public void creditQuestions1(String country, String dependents, String phonetype2, String area2, String number2) {
        newClientPage.selectCountryOfBirthPY(country);
        //dependens
        newClientPage.setInputPY(dependents, "3");
        newClientPage.selectPhoneTypePY("personalForm", "1", phonetype2, "5", area2, "5", "6", number2);
        newClientPage.sendDataStepPY("button-form-credit-qualification");
    }
    @Step("Preguntas para la evaluacion del credito step 1 en e2e")
    public void creditQuestions1e2e(String country, String dependents, String phonetype2, String area2, String number2) {
        newClientPage.selectCountryOfBirthPY(country);
        //dependens
        newClientPage.setInputPY(dependents, "2");
        newClientPage.selectPhoneTypePY("personalForm", "1", phonetype2, "4", area2, "4", "5", number2);
        newClientPage.sendDataStepPY("button-form-credit-qualification");
    }
    @Step("Preguntas para la evaluacion del credito step 2")
    public void creditQuestions2(String company, String year, String month, String day, String ocupation, String departament,
                                 String district, String barrio, String calle, String referencia,
                                 String phoneTypeWork1, String phoneAreaWork1, String phoneNumberWork1,
                                 String phoneTypeWork2, String phoneAreaWork2, String phoneNumberWork2) {
        newClientPage.setInputPY(company,"1");
        newClientPage.datePY(year,month,day);
        //Ocupacion
        newClientPage.selectOcupationPY(ocupation);
        //Departamento
        newClientPage.selectDeparmentPY(departament, "1");
        newClientPage.selectDistrictPY(district, "2");
        newClientPage.selectNeighborhoodPY(barrio, "3");
        newClientPage.setTextAreaPY(calle, "1");
        newClientPage.setInputPY(referencia, "4");
        // Telefono trabajo 1
        newClientPage.selectPhoneTypePY("ocupationForm","4", phoneTypeWork1, "10", phoneAreaWork1, "6", "7", phoneNumberWork1);
        // Telefono trabajo 2
        newClientPage.selectPhoneTypePY("ocupationForm","5", phoneTypeWork2, "14", phoneAreaWork2, "9", "10", phoneNumberWork2);

        newClientPage.sendDataStepPY("button-form-credit-qualification");
    }
    @Step("Preguntas para la evaluacion del credito step 3")
    public void creditQuestions3 (String calleResidencia, String refResidencia, String anio, String mes, String tipo) {
        newClientPage.setTextAreaPY(calleResidencia, "1");
        newClientPage.setInputPY(refResidencia, "1");
        newClientPage.firstDayHome(anio,mes);
        newClientPage.selectPY(tipo, "1");
        newClientPage.sendDataStepPY("button-form-credit-qualification");
    }
    @Step("Preguntas para la evaluacion del credito step 4")
    public void creditQuestions4 (String refContacto1, String parentesco1, String tipoTelefono1, String area1, String num1,
                                  String refContacto2, String parentesco2, String tipoTelefono2, String area2, String num2,
                                  String refContacto3, String parentesco3, String tipoTelefono3, String area3, String num3) {
        //Referencia de contacto 1
        newClientPage.setInputPY(refContacto1, "1");
        newClientPage.selectPY(parentesco1, "1");
        newClientPage.selectPhoneTypePY("contactForm", "2", tipoTelefono1, "4", area1, "3", "4", num1);
        // Referencia de contacto 2
        newClientPage.setInputPY(refContacto2, "5");
        newClientPage.selectPY(parentesco2, "3");
        newClientPage.selectPhoneTypePY("contactForm", "4", tipoTelefono2, "10", area2, "7", "8", num2);
        // Referencia de contacto 3
        newClientPage.setInputPY(refContacto3, "9");
        newClientPage.selectPY(parentesco3, "5");
        newClientPage.selectPhoneTypePY("contactForm", "6", tipoTelefono3, "16", area3, "11", "12", num3);

        newClientPage.sendDataStepPY("button-form-credit-qualification");
    }

    @Step("Preguntas para la evaluacion del credito step 5")
    public void creditQuestions5 (String typeofline) {
        newClientPage.typeoflinePY(typeofline);
        newClientPage.uploadImage("Frente del documento de identidad del cliente");
        newClientPage.uploadImage("Dorso del documento de identidad del cliente");
        newClientPage.uploadImage("Ingrese la boleta de ANDE");
        newClientPage.uploadImage("Ingrese un comprobante de sus ingresos");
        //newClientPage.uploadImage("Declaración de IVA");
        /*if (typeofline.contains("consumo")) {
            newClientPage.uploadImage("Patente comercial");
        }*/
        newClientPage.uploadImage("Documento microcrédito 1");
        newClientPage.uploadImage("Documento microcrédito 2");
        newClientPage.uploadImage("Documento microcrédito 3");
        newClientPage.uploadImage("Documento microcrédito 4");

        newClientPage.sendDataStepPY("button-form-credit-qualification");
        newClientPage.windowsFinal();
    }
}
