package org.unicomer.steps.emma;

import net.thucydides.core.annotations.Step;
import org.unicomer.pages.emma.LoanQuoterPage;

public class LoanQuoterStep {
    LoanQuoterPage loan;

    @Step("Search TRN to check loan")
    public void validateLoan(){
        loan.clickToCheck();
    }

    @Step("cash Loan amount")
    public void calculateQuote(){
        loan.insertAmount();
        loan.noCpi();
        loan.generateLoan();
        loan.waitLoanAlert();
    }

    public void loanQuoterAruba(String document){
        loan.selectDocumentType();
        loan.selectDocumentType(document);
        loan.clickToCheck();
    }

    public void calculateQuoteAruba(){
        loan.insertAmount();
        loan.generateLoan();
        loan.waitLoanAlert();
    }
}
