package org.unicomer.steps.mambu;

import net.thucydides.core.annotations.Step;
import org.unicomer.pages.mambu.LoginPage;

public class LoginPageStep {
    LoginPage login;

    @Step("Navigate to mambu")
    public void navigate(){
        login.navigate();
    }

    @Step("Login in to mambu")
    public void login(){
        login.username();
        login.password();
        login.submitCredentials();
    }
}
