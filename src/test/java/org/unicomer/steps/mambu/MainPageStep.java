package org.unicomer.steps.mambu;

import org.unicomer.pages.mambu.MainPage;

public class MainPageStep {

    private MainPage main;

    public void searchId(String id){
        main.searchId(id);
        main.selectId();
    }

    public void selectCashLoanAccount(){
        main.selectCashLoanAccount();
    }
}
