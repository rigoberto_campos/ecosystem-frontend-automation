package org.unicomer.steps.mambu;

import net.thucydides.core.annotations.Step;
import org.openqa.selenium.NoSuchElementException;
import org.unicomer.pages.mambu.ClientPage;

public class ClientPageStep {

    ClientPage client;

    @Step("Select Overview menu")
    public void selectOverview(){
        try {
            client.selectOverviewMenu();
            client.saveData("1");
            client.saveData("2");
        }catch(NoSuchElementException e){

        }

    }

    @Step("Select schedule sub menu")
    public void scheduleSubMenu(){
        client.scheduleSubMenu();
        client.selectAllCheck();
    }

    @Step("Select detail sub menu")
    public void detailSubMenu(){
        client.detailsSubMenu();
        client.getAccountAmount();
        //tableData();
    }

    @Step("Select transaction sub menu")
    public void transactionSubMenu(){
        client.transactionSubMenu();
    }
    @Step("Validate Repayment")
    public void validateRepayment(){
        client.validateRepayment();
    }

    @Step("Save type of channel")
    public void getTypeChannel(){
        client.actionMenu();
        client.getChannel();
    }

    @Step("Select pagination 200")
    public void pagination(){
        client.pagination(200);
    }

    @Step("Get the last repaiment entered")
    public void repaymentEntered(){
        client.repaymentEntered(1);
    }

    @Step("Select Closed Account menu")
    public void selectClosed(){
        client.closedAccountMenu();
    }

    @Step("Get name and fullname")
    public void getName() {
        client.getName();
    }

    @Step("Get Email")
    public void getEmail() {
        client.getEmail();
    }

    @Step("Get Document ID")
    public void getDocumentID() {
        client.getDocumentID();
    }

    public void tableData(){
        client.saveData("1");
        client.saveData("2");
        client.saveData("6");
    }

    @Step("Payment amount")
    public void clickRePayment(String amount){
        client.clickRePayment();
        client.paymentAmount(amount);
        client.applyRepayment();
    }

    @Step("Click close Account")
    public void clickCloseAccount(){
        client.clickCloseAccount();
    }


    @Step("PayOff amount")
    public void payBox(String amount){
        client.clickRePayment();
        client.paymentAmount(amount);
    }

    @Step("Select backdate option")
    public void selectBackDate(){
        client.clickSelectDate();
        client.clickBackDate();
    }

    @Step("Go to previous month")
    public void changeMonth(){
        client.previousMonth();
    }

    @Step("Payment day")
    public void selectDay(String day){
        client.monthDay(day);
    }

    @Step("Payment apply")
    public void paymentApply(){
        client.applyRepayment();
    }

    @Step ("Payment Error")
    public void paymentError(String amount){
        client.paymentErrorAmount(amount);
    }

    @Step("Close account")
    public void closeAccount(String day){
        client.closeAccount(day);
    }
    @Step("Payment Error and Close accouynt")
    public void paymentErrorAndClose(String amount){
        client.paymentErrorAndClose(amount);
    }


}
