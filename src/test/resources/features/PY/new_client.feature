@Paraguay
Feature: New client request Emma


  @e2e @Document
  Scenario: New credit application Paraguay
    Given Env 'py-e2e'
    When Login into Emma
    Then Select 'Paraguay' country in Emma
    Then Search client Paraguay Document Type: 'Cedula de identidad' and Document Id: '7000571'
    Then Evaluation Personal Information 'Línea de crédito de consumo', 'Nelson', 'Flores', 'Masculino', 'Divorciado', '1994', 'OCT', '21', '0991111223', 'Dependiente con IPS', 'Mensual', '10899255', 'ASUNCION', 'ASUNCION', 'SAJONIA'
    Then Credit questions Step 1 of 5 e2e 'Paraguay', '2', 'Teléfono fijo', '91', '111228'
    Then Credit questions Step 2 of 5 'Unicomer', '2012', 'MAY', '7', 'Arquitecto', 'CONCEPCION', 'CONCEPCION', 'SAN AGUSTIN', 'SALTA', '52', 'Teléfono fijo trabajo', '91', '111559', 'Teléfono móvil trabajo', '', '0991111224'
    Then Credit questions Step 3 of 5 'Salta', '123', '2000', 'JUN', 'Propia'
    Then Credit questions Step 4 of 5 'Ana Maria', 'Madre', 'Teléfono fijo referencia', '91', '111555', 'Agustin', 'Hijo/a', 'Teléfono móvil referencia', '', '0991115555', 'Nicolas', 'Amigo/a', 'Teléfono fijo referencia', '91', '111556'
    Then  Credit questions Step 5 of 5 'Línea de crédito de consumo'

    Then Search client Paraguay Document Type: 'Cedula de identidad' and Document Id: '7000572'
    Then Evaluation Personal Information 'Línea de crédito de consumo', 'Miguel', 'Campos', 'Masculino', 'Divorciado', '1994', 'OCT', '21', '0991111223', 'Dependiente con IPS', 'Mensual', '10899255', 'ASUNCION', 'ASUNCION', 'SAJONIA'
    Then Credit questions Step 1 of 5 e2e 'Paraguay', '2', 'Teléfono fijo', '91', '111228'
    Then Credit questions Step 2 of 5 'Unicomer', '2012', 'MAY', '7', 'Arquitecto', 'CONCEPCION', 'CONCEPCION', 'SAN AGUSTIN', 'SALTA', '52', 'Teléfono fijo trabajo', '91', '111559', 'Teléfono móvil trabajo', '', '0991111224'
    Then Credit questions Step 3 of 5 'Salta', '123', '2000', 'JUN', 'Propia'
    Then Credit questions Step 4 of 5 'Ana Maria', 'Madre', 'Teléfono fijo referencia', '91', '111555', 'Agustin', 'Hijo/a', 'Teléfono móvil referencia', '', '0991115555', 'Nicolas', 'Amigo/a', 'Teléfono fijo referencia', '91', '111556'
    Then  Credit questions Step 5 of 5 'Línea de crédito de consumo'
