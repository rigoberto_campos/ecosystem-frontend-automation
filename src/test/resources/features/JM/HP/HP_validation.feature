@Jamaica @HP @Before
#@Jamaica and @HP and @Before
Feature: Create CashLoan from new client in Jamaica

  @Document @validationHP
  Scenario: HP validation process
    Given Evidence client with "819337284" as document_id and "TRN" as document_type in Emma
    When Login into Emma
    Then Select "Jamaica" country in Emma
    Then Go to payment menu in Emma
    Then Select "TRN" as document type in Emma
    Then Insert "819337284" as document in Emma
    Then Complete hp information with "Default HP" as hp type and "6" as period time in Emma

    When Open and login in Mambu web app
    Then Looking for ID
