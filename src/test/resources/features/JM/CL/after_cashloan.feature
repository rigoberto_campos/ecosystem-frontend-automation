@Jamaica @CL @After @Document
#@Jamaica and @CL and @After
Feature: After disbursement CashLoan in Jamaica

  Scenario: Client credit validation Jamaica
    When Login into WorkFlow
    Then Select "Jamaica" country in WorkFlow
    Then Select "Request Tracking" menu in WorkFlow
    Then Validate client disbursement

  Scenario: After disbursement emma evidence
    Given Evidence client with "681893781" as document_id and "TRN" as document_type
    When Login into Emma
    Then Select "Jamaica" country
    Then Verify client is approved

    When Open and login in Mambu web app
    Then Looking for ID
    Then Details schedule and transaction from selected account
