@Jamaica
Feature: Review mambu steps

  @workflowBefore @assign @Document
  Scenario: Client credit validation Jamaica
    When Login into WorkFlow
    Then Select "Jamaica" country in WorkFlow
    Then Select "Time Monitoring" menu and assign request in WorkFlow "840376168"
    Then Select "Request Tracking" menu in WorkFlow
    Then Approved client request with document "840376168" in WorkFlow

  @workflowBefore @validation @flows @Document
  Scenario: Client credit validation Jamaica
    When Login into WorkFlow
    Then Select "Jamaica" country in WorkFlow
    Then Select "Request Tracking" menu in WorkFlow
    Then Approved client request with document "472618264" in WorkFlow