@Jm @Cashloan @meetingTest @IGT_MEETING
Feature: Create CashLoan with new client in Jamaica

  @OnlyClient @Document
  Scenario: New credit application Jamaica
    Given Env 'jm-uat'
    When Login into Emma
    Then Select "Jamaica" country in Emma
    Then Create new client in Emma
    Then Complete client personal information when client is pp "No" in Emma
    Then Complete client region information from "Jamaica" "Hanover" "Ramble" in Emma
    Then Search client status from "Jamaica" in Emma

  #@OnlyWF
  #Scenario: Client credit validation Jamaica
  #  When Login into WorkFlow
  #  Then Select "Jamaica" country in WorkFlow
  #  Then Select "Time Monitoring" menu and assign request in WorkFlow
  #  Then Select "Request Tracking" menu in WorkFlow
  #  Then Approved client request with document in WorkFlow

 # @Document
 # Scenario: Create loan quoter in Emma
 #   When Login into Emma
 #   Then Select "Jamaica" country in Emma
 #   Then Verify client is approved in Emma
    Then Go to main page in Emma
    Then Loan quoter with client TRN in Emma
    Then Go to main page in Emma
    Then Verify client is approved in Emma
    Then Go to credit status menu in Emma
    Then Select request parameters "TRN" document id and "Loan Status" as type in Emma
    Then Set loan params "Cash loan" as promotion with "6" months "COURTS MAY PEN" as store and "9902996" as vendor in Emma
  #9905502 try to use this ID

    Then Go to main page in Emma
    Then Go to credit status menu in Emma
#    Then Select request parameters "TRN" document id and "Post-sale Evaluation" as type in Emma
#    Then Close message for document ready in Emma

#    When Open and login in Mambu web app
#    Then Looking for ID
#    Then Details schedule and transaction from selected account
