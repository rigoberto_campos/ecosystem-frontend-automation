@Jamaica @CL @Before @oldClient
#@Jamaica and @CL and @Before and not @NewClient and @oldClient
Feature: Create CashLoan from new client in Jamaica

  Scenario: Create loan quoter in Emma
    Given Env 'jm-uat'
    Given Evidence client with "271529629" as document_id and "TRN" as document_type in Emma
    When Login into Emma
    Then Select "Jamaica" country in Emma
    Then Verify client is approved in Emma
    Then Loan quoter with client TRN in Emma

    Then Go to main page in Emma
    Then Verify client is approved in Emma
    Then Go to credit status menu in Emma
    Then Select request parameters "TRN" document id and "Loan Status" as type in Emma
    Then Set loan params "Cash loan" as promotion with "6" months "COURTS MAY PEN" as store and "9902996" as vendor in Emma
