@Aruba @CL @After
Feature: After disbursement CashLoan in Jamaica

  Scenario: After disbursement emma evidence
    Given Evidence client with "112060927" as document_id and "Passport" as document_type
    When Login into Emma
    Then Select "Aruba" country
    Then Verify client is approved

  Scenario: Client credit validation Jamaica
    When Login into WorkFlow
    Then Select "Aruba" country in WorkFlow
    Then Select "Request Tracking" menu in WorkFlow
    Then Validate client disbursement