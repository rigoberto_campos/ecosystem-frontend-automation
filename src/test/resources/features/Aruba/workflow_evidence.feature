@Aruba @Workflow
Feature: WORKFLOW

  @assign
  Scenario: Client credit validation Jamaica
    When Login into WorkFlow
    Then Select "Aruba" country in WorkFlow
    Then Select "Time Monitoring" menu and assign request in WorkFlow "586674982"
    Then Select "Request Tracking" menu in WorkFlow
    Then Approved client request with document "586674982" Aruba

  @validation
  Scenario: Request tracking client in WorkFlow Aruba
    When Login into WorkFlow
    Then Select "Aruba" country in WorkFlow
    Then Select "Request Tracking" menu in WorkFlow
    Then Approved client request with document "586674982" Aruba


