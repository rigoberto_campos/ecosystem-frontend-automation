@Aruba @CL @NewClient @Before
#@Aruba and @CL and @NewClient and @Before
Feature: Create CashLoan from new client in Aruba

  @ThisOne @Document
  Scenario: New credit application Aruba
    Given Env 'aw-uat'
    When Login into Emma
    Then Select "Aruba" country in Emma
    Then Create new client in Emma
    Then Complete client personal information when client is pp "No" in Emma
    Then Complete client region information from "Aruba" "Savaneta" "Cura Cabai" in Emma
    Then Search client status from "Aruba" in Emma

  @ThisOne @Document
  Scenario: Client credit validation Jamaica
    When Login into WorkFlow
    Then Select "Aruba" country in WorkFlow
    Then Select "Time Monitoring" menu and assign request in WorkFlow
    Then Select "Request Tracking" menu in WorkFlow
    Then Approved client request with document in WorkFlow

  Scenario: Create loan quoter in Emma
    When Login into Emma
    Then Select "Aruba" country in Emma
    Then Verify client is approved in Emma
    Then Loan quoter with client "Passport" document in Emma

    Then Go to main page in Emma
    Then Verify client is approved in Emma
    Then Go to credit status menu in Emma
    Then Select request parameters "Passport" document id and "Loan Status" as type in Emma
    Then Set loan params "Default Cash Loan" as promotion with "6" months "AMC Megastore" as store and "9999" as vendor in Emma

  Scenario: Request tracking client in WorkFlow Aruba
    When Login into WorkFlow
    Then Select "Aruba" country in WorkFlow
    Then Select "Request Tracking" menu in WorkFlow
    Then Approved client request with document in WorkFlow Aruba

  @Document
  Scenario: Validate client is approved
    When Login into Emma
    Then Select "Aruba" country
    Then Go to credit status menu
    Then Select request parameters "Passport" document id and "Post-sale Evaluation" as type
    Then Close message for document ready

    When Open and login in Mambu web app
    Then Looking for ID
    Then Details schedule and transaction from selected account
