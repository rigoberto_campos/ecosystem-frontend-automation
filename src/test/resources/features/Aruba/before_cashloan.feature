@Aruba @CL @Before
#@Aruba and @CL and @Before
Feature: Create CashLoan from new client in Jamaica

  @Document @ArubaClient
  Scenario: Create loan quoter in Emma
    Given Evidence client with "900091115" as document_id and "Passport" as document_type in Emma
    When Login into Emma
    Then Select "Aruba" country in Emma
    Then Verify client is approved in Emma
    Then Loan quoter with client "Passport" document in Emma

    Then Go to main page in Emma
    Then Verify client is approved in Emma
    Then Go to credit status menu in Emma
    Then Select request parameters "Passport" document id and "Loan Status" as type in Emma
    Then Set loan params "Default Cash Loan" as promotion with "6" months "AMC Megastore" as store and "9999" as vendor in Emma

    Then Go to main page in Emma
    Then Go to credit status menu in Emma
    Then Select request parameters "Passport" document id and "Post-sale Evaluation" as type in Emma
    Then Close message for document ready in Emma

    When Open and login in Mambu web app
    Then Looking for ID
    Then Details schedule and transaction from selected account
