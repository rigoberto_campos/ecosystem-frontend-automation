@Aruba
Feature: Review mambu steps

  @ArubaMambu @Document
  Scenario: validation client in Mambu before
    When Open and login in Mambu web app
    Then Looking for ID 'BJFW431'
    Then Pay account '322.4'
    Then Details schedule and transaction from selected account

  @ArubaMambu @Document
  Scenario: validation client in Mambu before
    When Open and login in Mambu web app
    Then Looking for ID 'CHTZ540'
    Then Pay account '48.26'
    Then Details schedule and transaction from selected account

  @ArubaMambu @Document
  Scenario: validation client in Mambu before
    When Open and login in Mambu web app
    Then Looking for ID 'DYTJ061'
    Then Pay account '48'
    Then Details schedule and transaction from selected account

  @ArubaMambu @Document
  Scenario: validation client in Mambu before
    When Open and login in Mambu web app
    Then Looking for ID 'GWGH153'
    Then Pay account '1505.91'
    Then Details schedule and transaction from selected account

  @ArubaMambu @Document
  Scenario: validation client in Mambu before
    When Open and login in Mambu web app
    Then Looking for ID 'PPFY815'
    Then Pay account '328.48'
    Then Details schedule and transaction from selected account

  @ArubaMambu @Document
  Scenario: validation client in Mambu before
    When Open and login in Mambu web app
    Then Looking for ID 'GEXQ912'
    Then Pay account '2798.87'
    Then Details schedule and transaction from selected account

  @ArubaMambu @Document
  Scenario: validation client in Mambu before
    When Open and login in Mambu web app
    Then Looking for ID 'YHRB565'
    Then Pay account '6986.03'
    Then Details schedule and transaction from selected account
